
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Row, Col, Container } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
} from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { useDispatch, useSelector } from "react-redux";
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import { getTotalMoney } from "./redux/apiRequest";


import ScrollToTop from './ScrollToTop';
import HeaderContainer from "./containers/Header";
import CartContainer from "./containers/Cart";
import FilterProductsContainer from "./containers/FilterProducts";
import FooterComponent from "./components/Footer";
import SearchContainer from './containers/Search';
import HomeContainer from "./containers/Home";
import config from "./config";
import Notification from "./components/Notification";
import ProductDetailContainer from './containers/ProductDetail/';
import Account from "./components/Account";
import AdminContainer from './containers/Admin/';
import ProductControl from "./containers/ProductControl";
import Statistical from './containers/Statistical';
import OrderControl from './containers/OrderControl';
import AccountControl from './containers/AccountControl';
import BrandControl from "./containers/BrandControl";
import CategoryControl from './containers/CategoryControl';
import BannerControl from './containers/BannerControl';
import RoleControl from './containers/RoleControl';
import SettingControl from './containers/SettingControl';
import TransportControl from './containers/TransportControl';




function App() {
  const [currentUser, setCurrentUser] = useState(null);
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  const [data, setData] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [banners, setBanners] = useState([]);
  const [products, setProducts] = useState([]);
  const cart = useSelector((state) => state.cart); 
  const [pageSize, setPageSize] = useState(1);
  const [setting, setSetting] = useState({});
  const [adminPage, setAdminPage] = useState(false);

  const user = useSelector((state) => state.auth.login.currentUser); 

  const getProductSearch = async() => { 
    if (!value) {
        return setValue('');
    }

    await axios.get(`/api/search-product/${value}`)
        .then(res => setData(res))
        .catch(err => console.log(err))
  }

  useEffect(() => {
      getProductSearch();

      if (!value) 
          return setValue('');
  }, [value]);

  useEffect(() => {
      getTotalMoney(dispatch);
  }, [cart.cartItems, dispatch]);


  const handleSearch = (keyword) => {
      localStorage.setItem("seachKeyword", JSON.stringify(keyword));
      const slugProduct = hashSlug(keyword);

      setValue(slugProduct);
  }

  useEffect(() => {
    const value = JSON.parse(localStorage.getItem("seachKeyword"));
    setKeyword(value);
  }, [value]);



  const hashSlug = (name) => {
    
    if (name) {
        name = name.toLowerCase();     
    
        // xóa dấu
        name = name.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
        name = name.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
        name = name.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
        name = name.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
        name = name.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
        name = name.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
        name = name.replace(/(đ)/g, 'd');
    
        // Xóa ký tự đặc biệt
        name = name.replace(/([^0-9a-z-\s])/g, '');
    
        // Xóa khoảng trắng thay bằng ký tự -
        name = name.replace(/(\s+)/g, '-');
    
        // xóa phần dự - ở đầu
        name = name.replace(/^-+/g, '');
    
        // xóa phần dư - ở cuối
        name = name.replace(/-+$/g, '');
    }

    return name;
  }

  useEffect(() => {
    const getBanner = async() => {
     await axios.get("/api/get-all-banner")
      .then(res => setBanners(res?.Banners?.banners))
      .catch(err => console.log(err))
    }

    const getInfoCTy = async() => {
      await axios.get("/api/get-setting")
      .then(res => setSetting(res?.setting?.rows))
      .catch(err => console.log(err))
    }

    getBanner();
    getInfoCTy();
  }, []);



  useEffect(() => {
    const getProduct = async() => {
      await axios.get(`/api/get-all-product-panigation?page=${pageSize}`)
        .then(res => setProducts(res))
        .catch(err => console.log(err))
    }

    getProduct();
  }, [pageSize]);

  useEffect(() => {
    if (window.location.pathname === "/admin") {
        setAdminPage(true);
    } else {
      setAdminPage(false);
    }
  }, [adminPage]);

 
  return (
    <Router>
        <ToastContainer/>
        <ScrollToTop />
          <PayPalScriptProvider
            options={{"client-id": process.env.REACT_APP_PAYPAL_CLIENT_ID}}
          >
            {window.location.pathname.includes("/admin" )|| adminPage ? (
              <>
              <AdminContainer role={user?.user.roleId === 1} backUser={() => setAdminPage(false)}/>
                <Routes>
                  <Route exact path={config.routes.statistical} element={<Statistical/>}/>
                  <Route path={config.routes.productControl} element={<ProductControl/>}/>
                  <Route path={config.routes.orderControl} element={<OrderControl/>}/>
                  <Route path={config.routes.brandControl} element={<BrandControl/>}/>
                  <Route path={config.routes.categoryControl} element={<CategoryControl/>}/>
                  <Route path={config.routes.bannerControl} element={<BannerControl/>}/>
                  {user?.user.roleId === 1 && <Route path={config.routes.accountControl} element={<AccountControl/>}/>}
                  {user?.user.roleId === 1 && <Route path={config.routes.roleControl} element={<RoleControl/>}/>}
                  {user?.user.roleId === 1 && <Route path={config.routes.settingControl} element={<SettingControl/>}/>}
                  {user?.user.roleId === 1 && <Route path={config.routes.transportControl} element={<TransportControl/>}/>}
                </Routes>
              </>
            ) : (
              <Container fluid="auto">
              <HeaderContainer
                searchHeader={handleSearch}
                setting={setting}
                slug={value}
                keyword={keyword}
                adminPage={() => setAdminPage(true)}
                />
              
              <Routes>
                  <Route exact path={config.routes.home} element={<HomeContainer 
                    bannersData={banners}
                    productData={products}
                    getValue={(e) => setPageSize(Number(e.target.textContent))}
                  />}/>
                  <Route path={config.routes.notification} element={<Notification/>}/>
                  <Route path={config.routes.account} element={<Account/>}/>
                  <Route path={config.routes.search} element={<SearchContainer data={data}/>}/>
                  <Route path={config.routes.cart} element={<CartContainer/>}/>
                  <Route path={config.routes.productDetail} element={
                    <ProductDetailContainer 
                      data={value}
                      hashSlug={hashSlug}  
                    />
                  }/>
                  <Route path={config.routes.phone} element={<FilterProductsContainer/>}/>
                  <Route path={config.routes.laptop} element={<FilterProductsContainer/>}/>
                  <Route path={config.routes.accessory} element={<FilterProductsContainer/>}/>
              </Routes>

                <FooterComponent setting={setting}/>
            </Container>
            )}
          </PayPalScriptProvider>
      </Router>
  );
}

export default App;
