import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import RoleControlCom, { Edit, Add } from "../../components/RoleControl";
import Delete from "../../components/Delete/Delete";

function RoleControl() {
    const [product, setProduct] = useState([]);
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editBanner, setEditBanner] = useState([]);
    const [role, setRole] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getRole = async() => {
            axios.get(`/api/get-all-role`)
                .then(res => setRole(res))
                .catch(err => console.log(err))

            

                if (id) {
                    await axios.get(`/api/check-delete-role/${id}`)
                    .then(res => setSearch(res))
                    .catch(err => console.log(err))
                }
        }

        getRole()
    }, [edit, page, popup, add])

    const handleEdit = async(e) => {
        
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        await axios.get(`/api/get-role/${valueOfTd}`)
            .then(res => setEditBanner(res))
            .catch(err => console.log(err))

        setEdit(true);
    }

    const handleEditData = async(e) => {
        e.preventDefault();
        const {
            id,
            name,
            description
        } = document.forms[0];

        const data = {
            name: name.value,
            description: description.value
        }
        const brandtId = Number(id.value);


        await axios.put(`/api/edit-role/${brandtId}`, data)
            .then(res => setMessage(res))
            .catch(err => console.log(err))

        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }


    const handleConfirm = async() => {
        if (search) {
            if (search?.roles?.count === 0 || search?.roles?.length === 0) {
                await axios.delete("/api/delete-role", { data: {
                    id: id 
                }}, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))
                
                if (message === 0) {
                    toast.success("Xóa thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                }
            } else {
                toast.error("Không thể xóa vì có ít nhất 1 sản phẩm mang thương hiệu này!", {
                    position: 'top-center'
                });
    
            }
            setPopup(false);
        }
       

        // setPopup(false);
    } 


    const handleAdd = async(e) => {
        const roleName = role?.roles?.rows;

        e.preventDefault();
 
        const {
            name,
            description
        } = document.forms[1];


        if (name.value !== '' || description.value !== '') {
            const data = {
                name: name.value,
                description: description.value
            }

            const result = roleName.some(item => item.name.includes(name.value));

            if (!result) {
                await axios.post(`/api/create-new-role`, data)
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))

                if (message === 0) {
                    toast.success("Tạo thành công!", {
                        position: 'top-center'
                    });
                } 

                setAdd(false);
            } else {
                toast.error("Tên đã tồn tại!", {
                    position: 'top-center'
                });
            }

            
        } else {
            toast.error("Bạn chưa nhập đủ thông tin!", {
                position: 'top-center'
            });
        }

        

       
    }
    
    return (
        <>
            {!edit && 
                <RoleControlCom
                    role={role}
                    openEdit={handleEdit}
                    // getValue={e => setPage(e.target.textContent)}
                    removeItem={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={editBanner}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default RoleControl;