import SearchComponent from "../../components/Search";


function Search({ data }) {
    
    return (
        <SearchComponent 
            data={data}
        />
    );
}

export default Search;