import { useEffect } from "react";
import axios from "axios";

function UserProvider({ setCurrentUser }) {
    useEffect(() => {
        if (localStorage.getItem('user')) {
            // let data = JSON.parse(localStorage.getItem('user'));
            // console.log(data); 
            // if (data) {
                axios.get(`/api/get-user/3`)
                    .then(res => setCurrentUser(res.user))
                    .catch(err => console.log(err))
            // }
    }
        
    }, [setCurrentUser]);

    return null;
}

export default UserProvider;