import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import CategoryControlCom, { Edit, Add } from "../../components/CategoryControl";
import Delete from "../../components/Delete/Delete";

function CategoryControl() {
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editCate, setEditCate] = useState([]);
    const [category, setCategory] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getCategory = async() => {
            axios.get(`/api/get-all-category`)
                .then(res => setCategory(res))
                .catch(err => console.log(err))

                if (id) {
                    await axios.get(`/api/check-category/${id}`)
                    .then(res => setSearch(res))
                    .catch(err => console.log(err))
                }
        }

        getCategory()
    }, [edit, page, popup, add])

    const handleEdit = async(e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        await axios.get(`/api/get-category/${valueOfTd}`)
            .then(res => setEditCate(res))
            .catch(err => console.log(err))

        

        setEdit(true);
    }

    useEffect(() => {
        const getData = async() => {
            await axios.get(`/api/get-all-category`)
                .then(res => setCategory(res))
                .catch(err => console.log(err))
        }
        getData();
    }, [add, edit]);

    const handleEditData = async(e) => {
        e.preventDefault();
        const {
            id,
            name,
            active,
        } = document.forms[0];


        const data = {
            name: name.value,
            is_active: Number(active.value),
        }
        const brandtId = Number(id.value);

        console.log(data);


        await axios.put(`/api/edit-category/${brandtId}`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))


        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }


    const handleConfirm = async() => {
        console.log(search);
        if (search) {
            if (search?.category?.count === 0 || search?.category?.length === 0) {
                await axios.delete("/api/delete-category", { data: {
                    id: id 
                }}, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))
                
                if (message === 0) {
                    toast.success("Xóa thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                }
        
                setPopup(false);
            } else {
                toast.error("Không thể xóa vì có ít nhất 1 sản phẩm mang thương hiệu này!", {
                    position: 'top-center'
                });
    
            }
            setPopup(false);
        }

        

        
    }

    
    const handleAdd = async(e, description) => {
        const categoryName = category?.cates;
        e.preventDefault();

        const {
            name,
            active,
        } = document.forms[1];

        if (name.value !== '') {
            const data = {
                name: name.value,
                is_active: active.value,
            }

            const result = categoryName.some(item => item.name.includes(name.value));
            
            if (!result) {
                await axios.post(`/api/create-new-category`, data)
                    .then(res => setMessage(res.errCode))
                    .catch(err => console.log(err))

                if (message === 0) {
                    toast.success("Cập nhật thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                }

                setAdd(false);
            } else {
                toast.error("Tên đã tồn tại!", {
                    position: 'top-center'
                });
            }
        } else {
            toast.error("Bạn chưa nhập đủ thông tin!", {
                position: 'top-center'
            });
        }
    }
    
    return (
        <>
            {!edit && 
                <CategoryControlCom
                    categories={category}
                    openEdit={handleEdit}
                    // getValue={e => setPage(e.target.textContent)}
                    removeCate={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={editCate}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default CategoryControl;