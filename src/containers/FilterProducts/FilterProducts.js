import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

import FilterProductsComponent from "../../components/FilterProducts";

function FilterProducts() {
    const path = window.location.pathname;
    const [data, setData] = useState(null);
    const [brand, setBrand] = useState([]);
    
    useEffect(() => {
        if (path === "/laptop") {
            //  axios.get("/api/get-product-cate/2")
            //     .then(res => setData(res.product))
            //     .catch(err => console.log(err))
                setData(2);

                axios.get("/api/get-brand-cate/9")
                .then(res => setBrand(res.brands))
                .catch(err => console.log(err))
        }   

        if (path === "/phone") {
            //  axios.get("/api/get-product-cate/1")
            //     .then(res => setData(res.product))
            //     .catch(err => console.log(err))
                setData(1);

                axios.get("/api/get-brand-cate/10")
                .then(res => setBrand(res.brands))
                .catch(err => console.log(err))
        }

        if (path === "/accessory") {
            //  axios.get("/api/get-product-cate/3")
            //     .then(res => setData(res.product))
            //     .catch(err => console.log(err))
                setData(3);

                axios.get("/api/get-brand-cate/11")
                .then(res => setBrand(res.brands))
                .catch(err => console.log(err))
        }
    }, [path]);

    console.log(brand);

    return (
        <FilterProductsComponent 
            data={data}
            brands={brand}
        />
    );
}

export default FilterProducts;