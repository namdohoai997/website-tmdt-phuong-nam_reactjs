import bcrypt from "bcryptjs";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import Me, { ChangInfo } from "../../components/Me";
import HeaderComponent from "../../components/Header";
import { Login, Cart, Destroy, Register, Confirm, OrderDetail } from "../../components/Account";
import { loginUser, registerUser } from "../../redux/apiRequest";
import { logOut } from "../../redux/apiRequest";
import axios from "axios";

function Header({
    slug = '',
    keyword = '',
    setting = [],
    adminPage = () => {},
    searchHeader = () => {},
}) {
    const user = useSelector((state) => state.auth.login.currentUser); 
    const cart = useSelector((state) => state.cart); 
    const accessToken = user?.accessToken;
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [login, setLogin] = useState(false);
    const [register, setRegister] = useState(false);
    const [gender, setGender] = useState(null);
    const [selectedFile, setSelectedFile] = useState(null);
    const [preview, setPreview] = useState();
    const [me, setMe] = useState(false);
    const [cartUser, setCartUser] = useState(false);
    const [orderId, setOrderId] = useState('');
    const [transport, setTransport] = useState([]);
    const [destroy, setDestroy] = useState(false);
    const [detailId, setDetailId] = useState(0);
    const [success, setSuccess] = useState(0);
    const [product, setProduct] = useState({});
    const [quantity, setQuantity] = useState(0);
    const [confirm, setConfirm] = useState(false);
    const [id, setId] = useState(null);
    const [detailList, setDetailList] = useState([]);
    const [detail, setDetail] = useState(false);
    const [errCode, setErrCode] = useState(0);
    const [change, setChange] = useState(false);

    
    localStorage.setItem("refreshToken", JSON.stringify(user?.refreshToken));

    const handleLogout = () => {
        logOut(dispatch, navigate, accessToken);
        localStorage.removeItem("persist:root");
    }

    const handleForm = () => {
        setLogin(true);
    }

    const handleClose = () => {
        setLogin(false);
    }

    const handleLogin = (e) => {
        e.preventDefault();

        let { email, password } = document.forms[1];


        const data = {
            email: email.value,
            password: password.value
        }

        loginUser(data, dispatch, navigate);
        setLogin(false);
    }

    
    const handleRegister = (e) => {
        e.preventDefault();

        const { 
            fname,
            lname,
            address,
            email, 
            pnumber,
            userName,
            password,
        } = document.forms[1];


        if (fname.value && lname.value && address.value && email.value && userName.value && gender && password.value && pnumber.value) {
            const data = {
                firstName: fname.value,
                lastName: lname.value,
                address: address.value,
                email: email.value,
                gender: Number(gender.value),
                userName: userName.value,
                password: password.value,
                phoneNumber: pnumber.value,
                roleId: 3,
                image: preview
            }
    
            registerUser(data, dispatch);

            setConfirm(true);
        } else {
            toast.warn("Bạn chưa nhập đủ thông tin!", {
                position: 'top-center'
            });
        }

        
    }

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile) 
        setPreview(objectUrl)

        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const handleAvatar = (e) => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }

        setSelectedFile(e.target.files[0])
    }

    const genderUser = (value) => {
        console.log(value);
        setGender(value);
    }

    // console.log(gender);

    const handleGetInfo = () => {
        setMe(true);
    }

    const handleGetCart = () => {
        setCartUser(true);
    }

    useEffect(() => {
        const userId = user?.user?.id;

        axios.get(`/api/get-order-user/${userId}`)
            .then(res => setOrderId(res.order))
            .catch(err => console.log(err))

            axios.get(`/api/get-all-transport`)
            .then(res => setTransport(res?.Transports.rows))
            .catch(err => console.log(err))
        
    }, [cartUser])
    
    const handleDestroyOrder = async(id, productId, quantity) => {
        setDestroy(true);
        setDetailId(id);
        setQuantity(quantity);

        await axios.get(`/api/get-product-id/${productId}`)
            .then(res => setProduct(res))
            .catch(err => console.log(err))
    }
    

    
    const handleConfirm = async() => {  
        const id = product?.product[0]?.id;

        const data = {
            total_products: Number(product?.product[0]?.total_products) + Number(quantity)
        }

        await axios.put(`/api/edit-product/${id}`, data)
            .then(res => setSuccess(res?.errCode))
            .catch(err => console.log(err))

        await axios.put(`/api/edit-orderDetail/${detailId}`, { order_status: 4 })
            .then(res => setSuccess(res?.errCode))
            .catch(err => console.log(err))

        if (success === 0) {
            toast.success("Hủy đơn hàng thành công!", {
                position: "top-center"
            });
            
        } else {
            toast.error("Có lỗi xảy ra!", {
                position: "top-center"
            });
        }

        setDestroy(false);
    }
  


    const registerForm = () => {
        setLogin(false);
        setRegister(true);
    }

    const LoginForm = () => {
        setLogin(true);
        setRegister(false);
        setConfirm(false);
    }

    const handleGetValue = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;
  
        var valueOfTd = target.querySelector('td:first-child').innerText;
  
        setId(valueOfTd);
        
        setDetail(true);
      }
  
  
      useEffect(() => {
        const getAPI = () => {
          if (id) {
  
            axios.get(`/api/search-orderDetail/${id}`)
            .then(res => setDetailList(res))
            .catch(err => console.log(err))
          }
        }
  
        getAPI();
      }, [id, detail, destroy]);

      const handleChange = async(e, gender) => {
        e.preventDefault();

        const {
            id,
            fname,
            lname,
            address,
            email,
            pnumber,
            password,
            avatar
        } = document.forms[1]


        
        const data = {
            id: Number(id.value),
            firstName: fname.value,
            lastName: lname.value,
            address: address.value,
            email: email.value,
            gender: Number(gender),
            password: password.value,
            phoneNumber: pnumber.value,
            image: avatar.value
        }

        // console.log(data);

        await axios.put("/api/edit-user", data)
        .then(res => setErrCode(res?.errCode))
        .catch(err => console.log(err))

       
        
        const newData = {
            email: email.value,
            password: "anhnamvh1999"
        }

        loginUser(newData, dispatch, navigate);

        console.log(errCode);


        if (errCode === 0) {
            toast.success("Thay đổi thông tin thành công!", {
                position: "top-center"
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: "top-center"
            });
        }

        setChange(false);
        setMe(true);
    }

  

    return (
        <>
            <HeaderComponent 
                user={user}
                cart={cart}
                setting={setting}
                logout={handleLogout}
                searchItem={searchHeader}
                slug={slug}
                search={keyword}
                accountForm={handleForm}
                getInfo={handleGetInfo}
                getCart={handleGetCart}
                adminPage={adminPage}
            />

            {me && <Me 
                data={user}
                show={me}
                setShow={() => setMe(false)}
                setChange={() => setChange(true)}
            />}

            {change && <ChangInfo
                show={change}
                data={user}
                selectedFile={selectedFile}
                preview={preview}
                setShow={() => setChange(false)}
                handleChange={handleChange}
            />}

            {cartUser && <Cart 
                display={cartUser}
                setShow={() => setCartUser(false)}
                data={orderId}
                transports={transport}
                getValue={handleGetValue}
            />}

            {login && <Login 
                show={login}
                closeButton={handleClose}
                loginButton={handleLogin}
                registerButton={registerForm}
            />}



            {register && <Register
                show={register}
                selectedFile={selectedFile}
                preview={preview}
                closeButton={() => setRegister(false)}
                registerButton={handleRegister}
                setGender={genderUser}
                handleAvatar={handleAvatar}
                loginButton={LoginForm}
            />}

            {destroy && <Destroy
                show={destroy}
                handleClose={() => setDestroy(false)}
                handleConfirm={handleConfirm}
            />}

            {confirm && <Confirm
                show={confirm}
                handleClose={() => setConfirm(false)}
                handleConfirm={LoginForm}
            />}

            {detail && <OrderDetail
                display={detail}
                detailList={detailList}
                detroyOrder={handleDestroyOrder}
                handleDisplay={() => setDetail(false)}
            />}
        </>
    );
}

export default Header;