import axios from "axios";
import { useEffect, useState } from "react";

import StatisticalComponent from "../../components/Statistical";

function Statistical() {
    const [month, setMonth] = useState([]);
    const [products, setProducts] = useState([]);
    const [count, setCount] = useState(1);
    const [money, setMoney] = useState([]);
    const [product, setProduct] = useState([]);
    const [brand, setBrand] = useState([]);


    useEffect(() => {
        const getData = async() => {
            await axios.get("/api/get-month-order")
                .then(res => setMonth(res?.order))
                .catch(err => console.log(err))

            await axios.get(`/api/get-most-product/${count}`)
                .then(res => setProducts(res?.orderDetail))
                .catch(err => console.log(err))

            await axios.get("/api/get-all-product")
                .then(res => setProduct(res?.products?.count))
                .catch(err => console.log(err))

            await axios.get("/api/get-brand")
                .then(res => setBrand(res?.brands?.brands))
                .catch(err => console.log(err))

            await axios.get("/api/get-money")
                .then(res => setMoney(res?.orderDetail))
                .catch(err => console.log(err))
        }

        getData();
    }, [count]);

    console.log(brand);


    const handleSetMonth = (value) => {
        setCount(value);
    }

    return (
        <StatisticalComponent
            month={month}
            products={products}
            // count={count}
            money={money}
            setMonth={handleSetMonth}
            product={product}
            brand={brand}
        />
    );
}

export default Statistical;