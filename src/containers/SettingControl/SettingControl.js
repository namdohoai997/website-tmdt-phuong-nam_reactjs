import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import SettingControlComponent, { Edit, Add } from "../../components/SettingControl";
import Delete from "../../components/Delete/Delete";

function SettingControl() {
    const [setting, setSetting] = useState([]);
    const [findSetting, setFindSetting] = useState([]);
    const [other, setOther] = useState([]);
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editProduct, setEditProduct] = useState([]);
    const [role, setRole] = useState([]);
    const [brand, setBrand] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getProduct = async() => {
            axios.get(`/api/get-setting`)
                .then(res => setSetting(res))
                .catch(err => console.log(err))
        }

        getProduct()
    }, [edit, page, popup, add])

    const handleEdit = () => {
        // await axios.get(`/api/edit-setting/${id}`)
        //     .then(res => setFindSetting(res))
        //     .catch(err => console.log(err))
        setEdit(true);
    }

    useEffect(() => {
        const getData = async() => {
            await axios.get(`/api/get-all-role`)
                .then(res => setRole(res))
                .catch(err => console.log(err))
        }
        getData();
    }, [add, edit]);

    const handleEditData = async(e) => {
        e.preventDefault();
        const {
            id,
            name,
            logo,
            address,
            email,
            phone,
            description,
            fax
        } = document.forms[0];




        const data = {
            name: name.value,
            address: address.value,
            email: email.value,
            description: description.value,
            phone: Number(phone.value),
            fax: Number(fax.value),
            logo: logo.value
        }

        const idSetting = Number(id.value);

        await axios.put(`/api/edit-setting/${idSetting}`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))

        if (message !== 0) {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        } else {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }

    console.log(popup);

    const handleConfirm = async() => {
        await axios.delete("/api/delete-user", { data: {
            id: id 
        }}, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => setMessage(res.errCode))
        .catch(err => console.log(err))
        
        if (message !== 0) {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        } else {
            toast.success("Xóa thành công!", {
                position: 'top-center'
            });
        }

        setPopup(false);
    }

    const handleAdd = async(e, description) => {
        e.preventDefault();

        const {
            lname,
            fname,
            image,
            address,
            email,
            phone,
            gender,
            roleId,
            password
        } = document.forms[2];


        const data = {
            firstName: fname.value,
            lastName: lname.value,
            address: address.value,
            email: email.value,
            password: password.value,
            gender: gender.value,
            phoneNumber: Number(phone.value),
            roleId: Number(roleId.value),
            image: image.value
        }

        await axios.post(`/api/create-new-user`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))

        if (message === 0) {
            toast.success("Thêm thành công thành công!", {
                position: 'top-center'
            });
            setAdd(false);
        } else if (message === 1) {
            toast.success("Email đã tồn tại!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
            setAdd(false);
        }

        
    }
    
    return (
        <>
            {!edit && 
                <SettingControlComponent
                    setting={setting}
                    // others={other}
                    openEdit={handleEdit}
                    // getValue={e => setPage(e.target.textContent)}
                    // removeItem={handleRemove}
                    // openAdd={() => setAdd(true)}
                />
            }

            {/* {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                roles={role}
                sendData={handleAdd}
            />} */}

            {/* {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            } */}

            {edit && 
                <Edit
                    show={edit}
                    data={setting}
                    // roles={role}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default SettingControl;