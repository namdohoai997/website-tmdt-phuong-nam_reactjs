import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import ProductControlCom, { Edit, Add } from "../../components/ProductControl";
import Delete from "../../components/Delete/Delete";

function ProductControl() {
    const [product, setProduct] = useState([]);
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editProduct, setEditProduct] = useState([]);
    const [category, setCategory] = useState([]);
    const [brand, setBrand] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getProduct = async() => {
            axios.get(`/api/get-all-product-panigation?page=${page}`)
                .then(res => setProduct(res))
                .catch(err => console.log(err))

            if (id) {
                await axios.get(`/api/check-product/${id}`)
                    .then(res => setSearch(res))
                    .catch(err => console.log(err))
            }
        }

        getProduct()
    }, [edit, page, popup, add])


    const hashSlug = (name) => {
    
        if (name) {
            name = name.toLowerCase();     
        
            // xóa dấu
            name = name.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
            name = name.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
            name = name.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
            name = name.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
            name = name.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
            name = name.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
            name = name.replace(/(đ)/g, 'd');
        
            // Xóa ký tự đặc biệt
            name = name.replace(/([^0-9a-z-\s])/g, '');
        
            // Xóa khoảng trắng thay bằng ký tự -
            name = name.replace(/(\s+)/g, '-');
        
            // xóa phần dự - ở đầu
            name = name.replace(/^-+/g, '');
        
            // xóa phần dư - ở cuối
            name = name.replace(/-+$/g, '');
        }
    
        return name;
      }

    const handleEdit = async(e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;


        await axios.get(`/api/get-product-id/${valueOfTd}`)
            .then(res => setEditProduct(res))
            .catch(err => console.log(err))

        

        setEdit(true);
    }

    useEffect(() => {
        const getData = async() => {
            await axios.get(`/api/get-all-category`)
                .then(res => setCategory(res))
                .catch(err => console.log(err))

            await axios.get(`/api/get-brand`)
                .then(res => setBrand(res))
                .catch(err => console.log(err))
        }
        getData();
    }, [add, edit]);

    const handleEditData = async(e, description) => {
        e.preventDefault();
        const {
            id,
            name,
            image,
            price,
            quantity,
            hot,
            active,
            brand,
            sale,
            category,
        } = document.forms[0];



        const data = {
            name: name.value,
            slug: hashSlug(name.value),
            cate_id: Number(category.value),
            price: Number(price.value),
            sale: Number(sale.value),
            is_hot: Number(hot.value),
            description: description,
            image: image.value,
            total_products: Number(quantity.value),
            is_active: active.value,
            brand_id: Number(brand.value)
        }


        const productId = Number(id.value);

        await axios.put(`/api/edit-product/${productId}`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))

        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }

    const handleConfirm = async() => {
        if (search) {
            if (search?.product[0].count === 0) {
                await axios.delete("/api/delete-product", { data: {
                    id: id 
                }}, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))
                
                if (message === 0) {
                    toast.success("Xóa thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                }
        
                setPopup(false);
            } else {
                toast.error("Không thể xóa vì có ít nhất 1 giỏ hàng đang chứa sản phẩm này!", {
                    position: 'top-center'
                });

                setPopup(false);
            }
        }
    }


    const handleAdd = async(e, description) => {
        e.preventDefault();

        const {
            name,
            image,
            price,
            quantity,
            hot,
            active,
            brand,
            sale,
            category,
        } = document.forms[1];


        const data = {
            name: name.value,
            slug: hashSlug(name.value),
            cate_id: Number(category.value),
            price: Number(price.value),
            sale: Number(sale.value),
            is_hot: Number(hot.value),
            description: description,
            image: image.value,
            total_products: Number(quantity.value),
            is_active: active.value,
            brand_id: Number(brand.value)
        }

        await axios.post(`/api/create-new-product`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))

        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }

        setAdd(false);
    }
    
    return (
        <>
            {!edit && 
                <ProductControlCom
                    products={product}
                    openEdit={handleEdit}
                    getValue={e => setPage(e.target.textContent)}
                    removeProduct={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                brand={brand}
                category={category}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={editProduct}
                    category={category}
                    brand={brand}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default ProductControl;