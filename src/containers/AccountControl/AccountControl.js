import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import AccountControlComponent, { Edit, Add } from "../../components/AccountControl";
import Delete from "../../components/Delete/Delete";

function AccountControl() {
    const [user, setUser] = useState([]);
    const [findUser, setFindUser] = useState([]);
    const [other, setOther] = useState([]);
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editProduct, setEditProduct] = useState([]);
    const [role, setRole] = useState([]);
    const [brand, setBrand] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getProduct = async() => {
            axios.get(`/api/get-all-user/${3}?page=${page}`)
                .then(res => setUser(res))
                .catch(err => console.log(err))

            axios.get(`/api/get-all-user/${2}?page=${page}`)
                .then(res => setOther(res))
                .catch(err => console.log(err))
        }

        getProduct()
    }, [edit, page, popup, add])

    const handleEdit = async(e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;


        await axios.get(`/api/get-user/${valueOfTd}`)
            .then(res => setFindUser(res))
            .catch(err => console.log(err))

        

        setEdit(true);
    }

    useEffect(() => {
        const getData = async() => {
            await axios.get(`/api/get-all-role`)
                .then(res => setRole(res))
                .catch(err => console.log(err))
        }
        getData();
    }, [add, edit]);

    const handleEditData = async(e, description) => {
        e.preventDefault();
        const {
            id,
            lname,
            fname,
            address,
            image,
            email,
            phone,
            gender,
            roleId
        } = document.forms[0];

        console.log(document.forms[0]);



        const data = {
            id: Number(id.value),
            firstName: fname.value,
            lastName: lname.value,
            address: address.value,
            email: email.value,
            gender: gender.value,
            phoneNumber: Number(phone.value),
            roleId: Number(roleId.value),
            image: image.value
        }

        await axios.put(`/api/edit-user`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))

        if (message !== 0) {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        } else {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }


    const handleConfirm = async() => {
        await axios.delete("/api/delete-user", { data: {
            id: id 
        }}, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => setMessage(res.errCode))
        .catch(err => console.log(err))
        
        if (message !== 0) {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        } else {
            toast.success("Xóa thành công!", {
                position: 'top-center'
            });
        }

        setPopup(false);
    }

    const handleAdd = async(e, description) => {
        e.preventDefault();

        const userEmail = user?.user?.users;
        const otherEmail = other?.user?.users;

        const {
            lname,
            fname,
            image,
            address,
            email,
            phone,
            gender,
            roleId,
            password
        } = document.forms[2];

        if (fname.value !== '' || lname.value !== '' || address.value !== '' || email.value !== '' || password.value !== '' || phone.value !== '') {

            const data = {
                firstName: fname.value,
                lastName: lname.value,
                address: address.value,
                email: email.value,
                password: password.value,
                gender: gender.value,
                phoneNumber: Number(phone.value),
                roleId: Number(roleId.value),
                image: image.value
            }

            const resultUser = userEmail.some(item => item.email.includes(email.value));
            const resultOther = otherEmail.some(item => item.email.includes(email.value));

            if (!resultOther && !resultUser) {

                await axios.post(`/api/create-new-user`, data)
                    .then(res => setMessage(res.errCode))
                    .catch(err => console.log(err))

                if (message === 0) {
                    toast.success("Thêm thành công thành công!", {
                        position: 'top-center'
                    });
                    setAdd(false);
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                    setAdd(false);
                }
            } else {
                toast.error("Email đã tồn tại!", {
                    position: 'top-center'
                });
            }
        } else {
            toast.error("Bạn chưa nhập đủ thông tin!", {
                position: 'top-center'
            });
        }
        
    }
    
    return (
        <>
            {!edit && 
                <AccountControlComponent
                    users={user}
                    others={other}
                    openEdit={handleEdit}
                    getValue={e => setPage(e.target.textContent)}
                    removeItem={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                roles={role}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={findUser}
                    roles={role}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default AccountControl;