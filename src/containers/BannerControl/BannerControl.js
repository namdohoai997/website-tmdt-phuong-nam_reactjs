import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import BannerControlCom, { Edit, Add } from "../../components/BannerControl";
import Delete from "../../components/Delete/Delete";

function BannerControl() {
    const [product, setProduct] = useState([]);
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editBanner, setEditBanner] = useState([]);
    const [banner, setBanner] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getBanner = async() => {
            axios.get(`/api/get-all-banner?page=${page}`)
                .then(res => setBanner(res))
                .catch(err => console.log(err))

                if (id) {
                    await axios.get(`/api/check-banner/${id}`)
                    .then(res => setSearch(res))
                    .catch(err => console.log(err))
                }
        }

        getBanner()
    }, [edit, page, popup, add])

    const handleEdit = async(e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        await axios.get(`/api/get-banner/${valueOfTd}`)
            .then(res => setEditBanner(res))
            .catch(err => console.log(err))

        

        setEdit(true);
    }

    const handleEditData = async(e) => {
        e.preventDefault();
        const {
            id,
            active,
            title,
            image,
            description
        } = document.forms[0];


        const data = {
            is_active: active.value,
            title: title.value,
            image: image.value,
            description: description.value
        }
        const brandtId = Number(id.value);

        console.log(data);

        await axios.put(`/api/edit-banner/${brandtId}`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))


        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }


    const handleConfirm = async() => {
        await axios.delete("/api/delete-banner", { data: {
            id: id 
        }}, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => setMessage(res.errCode))
        .catch(err => console.log(err))
        
        if (message === 0) {
            toast.success("Xóa thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }

        setPopup(false);
    } 


    const handleAdd = async(e) => {
        e.preventDefault();

        const {
            active,
            title,
            image,
            description
        } = document.forms[1];

        if (title.value !== '' || image.value !== '' || description.value !== '') {
            const data = {
                is_active: active.value,
                title: title.value,
                image: image.value,
                description: description.value
            }
    
            await axios.post(`/api/create-new-banner`, data)
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))
    
            if (message === 0) {
                toast.success("Cập nhật thành công!", {
                    position: 'top-center'
                });
            } else {
                toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                    position: 'top-center'
                });
            }

            setAdd(false);
        } else {
            toast.error("Bạn chưa nhập đủ thông tin!", {
                position: 'top-center'
            });
        }

        

       
    }
    
    return (
        <>
            {!edit && 
                <BannerControlCom
                    banner={banner}
                    openEdit={handleEdit}
                    getValue={e => setPage(e.target.textContent)}
                    removeCate={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={editBanner}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default BannerControl;