

import AdminComponent from "../../components/Admin";

function Admin({
    role = false,
    backUser = () => {}
}) {

    return (
        <AdminComponent 
            role={role} 
            backUser={backUser}
        />
    );
}

export default Admin;