import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

import { buyProductNow } from "../../redux/apiRequest";

import ProductDetailComponent from "../../components/ProductDetail";
// import { removeProductFromCart, decreaseValue, addProductToCart, deleteCart, orderSuccess } from "../../redux/apiRequest";



function ProductDetail() {

    const { slug } = useParams();
    const [data, setData] = useState({});
    const [value, setValue] = useState(1);
    const [isLogin, setIsLogin] = useState(false);
    const [products, setProducts] = useState({});
    const user = useSelector((state) => state.auth.login.currentUser);
    const dispatch = useDispatch();

    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3,
          slidesToSlide: 3 // optional, default to 1.
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2,
          slidesToSlide: 2 // optional, default to 1.
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        }
      };

    const hashSlug = (name) => {
    
        if (name) {
            name = name.toLowerCase();     
        
            // xóa dấu
            name = name.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
            name = name.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
            name = name.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
            name = name.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
            name = name.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
            name = name.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
            name = name.replace(/(đ)/g, 'd');
        
            // Xóa ký tự đặc biệt
            name = name.replace(/([^0-9a-z-\s])/g, '');
        
            // Xóa khoảng trắng thay bằng ký tự -
            name = name.replace(/(\s+)/g, '-');
        
            // xóa phần dự - ở đầu
            name = name.replace(/^-+/g, '');
        
            // xóa phần dư - ở cuối
            name = name.replace(/-+$/g, '');
        }
    
        return name;
    }

    useEffect(() => {
        if (!slug) {
            setData({});
        }

        const slugProduct = hashSlug(slug);

        axios.get(`/api/get-product/${slugProduct}`)
            .then(res =>setData(res))
            .catch(err => console.log(err))
    }, [slug]);


    const handleConvertMoneyFomat = price => {
        return price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
    }

    const handleIncrease = () => {
        setValue(prev => prev + 1);
    }

    const handleDecrease = () => {
        if (value === 0)
            return;
        
        setValue(prev => prev - 1);
    }

    useEffect(() => {
        if (user) {
            setIsLogin(true);
        } else {
            setIsLogin(false);
        }

    }, [isLogin]);

    const handleBuyNow = (product) => {
        if (isLogin) {
            product.value = value;
            console.log(product);
            buyProductNow(dispatch, product);
        } else {
            toast.error("Bạn cần đăng nhập để sử dụng chức năng này!", {
                position: "top-right"
            });
        }
    }

    const handleGetProductRelated = async(data) => {
        const brandId = data.product?.rows[0]?.brand_id;

        // console.log(brandId);
        await axios.get(`/api/get-product-brand/${brandId}`)
            .then(res => setProducts(res))
            .catch(err => console.log(err))
    }

    useEffect(() => {
        handleGetProductRelated(data);
    }, [data]);

    return (
        <ProductDetailComponent 
            data={data} 
            value={value}
            isLogin={isLogin}
            products={products}
            responsive={responsive}
            convertMoney={handleConvertMoneyFomat}
            increase={handleIncrease}
            decrease={handleDecrease}
            buyNow={handleBuyNow}
            getProductRelated={handleGetProductRelated}
        />
    );
}

export default ProductDetail;