import HomeComponent from "../../components/Home";

function Home({
    bannersData = [],
    productData = [],
    getValue = () => {}
}) {
    return (
        <HomeComponent 
            banners={bannersData}
            product={productData}
            getValue={getValue}
        />
    );
}

export default Home;