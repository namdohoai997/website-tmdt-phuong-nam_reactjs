import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import BrandControlCom, { Edit, Add } from "../../components/BrandControl";
import Delete from "../../components/Delete/Delete";

function BrandControl() {
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editBrand, setEditBrand] = useState([]);
    const [category, setCategory] = useState([]);
    const [brand, setBrand] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getBrand = async() => {
            axios.get(`/api/get-all-brand?page=${page}`)
                .then(res => setBrand(res))
                .catch(err => console.log(err))

                if (id) {
                    await axios.get(`/api/check-brand/${id}`)
                    .then(res => setSearch(res))
                    .catch(err => console.log(err))
                }
        }

        getBrand()
    }, [edit, page, popup, add])


    const handleEdit = async(e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;


        await axios.get(`/api/get-brand/${valueOfTd}`)
            .then(res => setEditBrand(res))
            .catch(err => console.log(err))

        

        setEdit(true);
    }

    useEffect(() => {
        const getData = async() => {
            await axios.get(`/api/get-all-category`)
                .then(res => setCategory(res))
                .catch(err => console.log(err))
        }
        getData();
    }, [add, edit]);

    const handleEditData = async(e, description) => {
        e.preventDefault();
        const {
            id,
            name,
            active,
            category,
        } = document.forms[0];

        const data = {
            name: name.value,
            category_id: Number(category.value),
            description: description,
            is_active: active.value,
        }

        // console.log(active.value);

        const brandtId = Number(id.value);

        await axios.put(`/api/edit-brand/${brandtId}`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))


        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }


    const handleConfirm = async() => {
        if (search) {
            if (search?.brands[0]?.count === 0) {
                await axios.delete("/api/delete-brand", { data: {
                    id: id 
                }}, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))
                
                if (message === 0) {
                    toast.success("Xóa thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                }
        
                setPopup(false);
            } else {
                toast.error("Không thể xóa vì có ít nhất 1 sản phẩm mang thương hiệu này!", {
                    position: 'top-center'
                });
    
                setPopup(false);
            }
        }

        

        
    }


    const handleAdd = async(e, description) => {
        const brandName = brand?.brands?.brands;
        e.preventDefault();

        const {
            name,
            active,
            category,
        } = document.forms[1];

        if (name.value !== '') {
            const data = {
                name: name.value,
                category_id: Number(category.value),
                description: description,
                is_active: active.value,
            }

            const result = brandName.some(item => item.name.includes(name.value));

            if (!result) {

                await axios.post(`/api/create-new-brand`, data)
                    .then(res => setMessage(res.errCode))
                    .catch(err => console.log(err))

                if (message === 0) {
                    toast.success("Cập nhật thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                } 

                setAdd(false);
            } else {
                toast.error("Tên đã tồn tại!", {
                    position: 'top-center'
                });
            }
        } else {
        toast.error("Bạn chưa nhập đủ thông tin!", {
            position: 'top-center'
        });
    }

        
    }
    
    return (
        <>
            {!edit && 
                <BrandControlCom
                    brands={brand}
                    openEdit={handleEdit}
                    getValue={e => setPage(e.target.textContent)}
                    removeBrand={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                category={category}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={editBrand}
                    category={category}
                    // brand={brand}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default BrandControl;