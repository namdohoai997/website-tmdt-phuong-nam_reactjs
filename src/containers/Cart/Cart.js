import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect, useRef, useState } from "react";
import axios from "axios";

import CartComponent, { NotFound } from "../../components/Cart";
import { removeProductFromCart, decreaseValue, addProductToCart, deleteCart, orderSuccess } from "../../redux/apiRequest";
import Delete from "../../Delete/Delete";


function Cart() {
    const user = useSelector((state) => state.auth.login.currentUser); 
    const [product, setProduct] = useState([]);
    const [description, setDescription] = useState('');
    const [transport, setTransport] = useState([]);
    const [price, setPrice] = useState(30000);
    const [idTransports, setIdTransports] = useState(2);
    const [productQuantity, setProductQuantity] = useState(0);
    const [clear, setClear] = useState(false);
    const [notFound, setNotFound] = useState(false);
    const cart = useSelector((state) => state.cart); 
    const dispatch = useDispatch();
    const priceRef = useRef();

    useEffect(() => {
       const getAPI = async() => {
            await axios.get('/api/get-all-transport')
                .then(res => setTransport(res?.Transports))
                .catch(err => console.log(err))

            await axios.get('/api/get-all-product')
                .then(res => setProduct(res?.products?.rows))
                .catch(err => console.log(err))
       }

       getAPI();
    }, []);


    function checkCart() {
        if (cart.cartItems.length === 0) {
            setNotFound(true);
        }
    }

    // console.log(cart.cartItems);

    // useEffect(() => {
    //     getTotalMoney(dispatch);
    // }, [cart.cartTotalQuantity, dispatch]);

    // const getQuantity = slug => {
    //     return axios.get(`/api/get-product/${slug}`)
    //             .then(res => console.log(res))
    //             .catch(err => console.log(err))
    // }


    


    const handleDelete = (product) => {
        removeProductFromCart(dispatch, product);
        checkCart();
    }

    const handleConvertMoneyFomat = price => {
        return price?.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
    }

    const handleChangeValue = () => {

    }

    const handleTransportId = async(id) => {
        let idTransport = Number(id);
        
        setIdTransports(idTransport);

        await axios.get(`/api/get-transport/${idTransport}`)
            .then(res => setPrice(res?.transport?.price))
            .catch(err => console.log(err));
    }

    const handleDecrease = (product) => {
        decreaseValue(dispatch, product);
    }
    
    const handleIncrease = (product) => {
        addProductToCart(dispatch, product);
        setProductQuantity(product.total_products);
    }

    const handleDeleteCart = () => {
        setClear(true);
    }

    const handleConfirm = () => {
        setClear(false);
        deleteCart(dispatch);
        checkCart();
    }

    const handleCancel = () => {
        setClear(false);
    } 

    const handleCheckOut = () => {
        saveOrder();
    }

    const saveOrder = async(id) => {
        const date = Date.now();
        const statusCode = {};

        const totalPrice = cart.cartTotalAmount + price;

        console.log(cart);


        const dataOrder = {
            id: `DH${date}${user.user.id}`,
            user_id: user.user.id,
            total_price: cart.cartTotalAmount,
            description: description,
            title: `Đơn hàng của ${user.user.userName}`,
            total_Orders: totalPrice,
            note: '',
            voucher_id: '',
            transport_id: idTransports 
        }


        await axios.post("/api/create-new-order", dataOrder)
            .then(res => statusCode.order = res.errCode)
            .catch(err => console.log(err));

        cart?.cartItems?.map(async(item) => {
            const priceSale = item.price - item.price * item.sale / 100; 

            const dataDetail = {
                product_id: item.id,
                unit_price: item.sale ? priceSale : item.price,
                quantity: item.cartQuantity,
                total_price: item.sale ? priceSale * item.cartQuantity : item.price * item.cartQuantity,
                order_id: `DH${date}${user.user.id}`,
                order_status: 1,
                email: user?.user?.email,
                name: item.name
            } 

            const dataCount = {
                total_products: item.total_products - item.cartQuantity
            }

            await axios.put(`/api/edit-product/${item.id}`, dataCount) 
                .then(res => console.log(res))
                .catch(err => console.log(err));

            
            await axios.post("/api/create-new-orderDetail", dataDetail)
                .then(res => console.log(res))
                .catch(err => console.log(err));
        })

        if (statusCode.order === 0) {
            orderSuccess(dispatch);
        }
        
    }

    const handleApprove = (data) => {
        // setPaidFor(true);

        if (data.status === 'COMPLETED') {
            orderSuccess(dispatch);
            setClear(false);
            checkCart();
        }
    }


    const handleDescription = (e) => {
        setDescription(e.target.value);
    }


    return (
        <>
            { notFound ? <NotFound/> : (
                <CartComponent 
                    data={cart}
                    deleteProduct={handleDelete}
                    convertMoneyFomat={handleConvertMoneyFomat}
                    channgeValue={handleChangeValue}
                    decrease={handleDecrease}
                    increase={handleIncrease}
                    deleteCart={handleDeleteCart}
                    checkOut={handleCheckOut}
                    description={handleDescription}
                    maxProduct={productQuantity}
                    transport={transport}
                    transportId={handleTransportId}
                    transportPrice={price}
                    paypal={priceRef.current}
                    approvePaypal={handleApprove}
                />
            ) }

            {clear && 
                <Delete 
                    data={null}
                    confirmDelete={handleConfirm}
                    cancelDelete={handleCancel}
                    show={clear}
                />
            }

        </>
    )
}

export default Cart;

