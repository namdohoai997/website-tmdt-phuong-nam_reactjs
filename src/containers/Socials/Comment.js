import React, { Component } from "react";
import { connect, Connect } from "react-redux";


class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    initFacebookSDK() {
        if (window.FB) {
            window.FB.XFBML.parse();
        }

        // let { language } = this.props;
        // let locale = language === LANGUAGES.VI ? 'vi_VN' : 'en_US'
        window.fbAsyncInit = function () {
            window.FB.init({
                appId: process.env.REACT_APP_FACEBOOK_APP_ID,
                cookie: true,  // enable cookies to allow the server to access
                // the session
                xfbml: true,  // parse social plugins on this page
                version: 'v2.5' // use version 2.1
            });
        };
        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = `//connect.facebook.net/vi_VN/sdk.js`;
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    componentDidMount() {
        this.initFacebookSDK();
    }

    render() {
        let { width, dataHref, numposts } = this.props;

        let value = window.location.href;
        let valuew = value.substring(22, value.length);

        return (
            <>
                <div 
                    className="fb-comments" 
                    data-href={`https://developers.facebook.com/docs/plugins/comments/${valuew}`}
                    data-width={900} 
                    data-numposts={5}>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {

    };
}

const mapDispatchToProps = state => {
    return {
        
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Comment)