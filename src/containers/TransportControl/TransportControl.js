import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import TransportControlCom, { Edit, Add } from "../../components/TransportControl";
import Delete from "../../components/Delete/Delete";

function TransportControl() {
    const [page, setPage] = useState(1);
    const [edit, setEdit] = useState(false);
    const [editTransport, setEditTransport] = useState([]);
    const [transport, setTransport] = useState([]);
    const [message, setMessage] = useState(0);
    const [popup, setPopup] = useState(false);
    const [remove, setRemove] = useState(false);
    const [id, setId] = useState(0);
    const [add, setAdd] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [search, setSearch] = useState([]);

    useEffect(() => {
        const getRole = async() => {
            axios.get(`/api/get-all-transport`)
                .then(res => setTransport(res))
                .catch(err => console.log(err))

            

                if (id) {
                    await axios.get(`/api/check-order/${id}`)
                    .then(res => setSearch(res))
                    .catch(err => console.log(err))
                }
        }

        getRole()
    }, [edit, page, popup, add])

    const handleEdit = async(e) => {
        
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        await axios.get(`/api/get-transport/${valueOfTd}`)
            .then(res => setEditTransport(res))
            .catch(err => console.log(err))

        setEdit(true);
    }

    const handleEditData = async(e) => {
        e.preventDefault();
        const {
            id,
            name,
            price,
            description
        } = document.forms[0];

        const data = {
            name: name.value,
            price: price.value,
            description: description.value
        }
        const transportId = Number(id.value);


        await axios.put(`/api/edit-transport/${transportId}`, data)
            .then(res => setMessage(res.errCode))
            .catch(err => console.log(err))


        if (message === 0) {
            toast.success("Cập nhật thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }
        setEdit(false);
    }

    const handleRemove = (e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        setId(valueOfTd);
        setPopup(true);
    }


    const handleConfirm = async() => {
        if (search) {
            if (search?.transport?.count === 0 || search?.transport?.length === 0) {
                await axios.delete("/api/delete-transport", { data: {
                    id: id 
                }}, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))
                
                if (message === 0) {
                    toast.success("Xóa thành công!", {
                        position: 'top-center'
                    });
                } else {
                    toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                        position: 'top-center'
                    });
                }
            } else {
                toast.error("Không thể xóa vì đơn vị vận chuyển này đang chuyển ít nhất 1 đơn hàng!", {
                    position: 'top-center'
                });
    
            }
            setPopup(false);
        }
    } 


    const handleAdd = async(e) => {
        const transportName = transport?.Transports?.rows;

        e.preventDefault();
 
        const { 
            name,
            price,
            description
        } = document.forms[1]; 


        if (name.value !== '' || description.value !== '' || price.value !== '') {
            const data = {
                name: name.value,
                price: price.value,
                description: description.value
            }

            const result = transportName.some(item => item.name.includes(name.value));

            if (!result) {
                await axios.post(`/api/create-new-transport`, data)
                .then(res => setMessage(res.errCode))
                .catch(err => console.log(err))

                if (message === 0) {
                    toast.success("Tạo thành công!", {
                        position: 'top-center'
                    });
                } 

                setAdd(false);
            } else {
                toast.error("Tên đã tồn tại!", {
                    position: 'top-center'
                });
            }

            
        } else {
            toast.error("Bạn chưa nhập đủ thông tin!", {
                position: 'top-center'
            });
        }

        

       
    }
    
    return (
        <>
            {!edit && 
                <TransportControlCom
                    transport={transport}
                    openEdit={handleEdit}
                    removeItem={handleRemove}
                    openAdd={() => setAdd(true)}
                />
            }

            {add && <Add
                show={add}
                handleClose={() => setAdd(false)}
                sendData={handleAdd}
            />}

            {popup && <Delete
                show={popup}
                handleClose={() => setPopup(false)}
                handleConfirm={handleConfirm}
            />
            }

            {edit && 
                <Edit
                    show={edit}
                    data={editTransport}
                    handleClose={() => setEdit(false)}
                    sendData={handleEditData}
                />
            }
        </>
    );
}

export default TransportControl;