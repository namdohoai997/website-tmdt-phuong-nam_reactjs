import axios from "axios";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

import OrderComponent, { Detail } from "../../components/OrderControl";


function OrderControl() {
    const [order, setOrder] = useState([]);
    const [page, setPage] = useState(1);
    const [detail, setDetail] = useState(false);
    const [data, setData] = useState([]);
    const [edit, setEdit] = useState(false);
    const [status, setStatus] = useState([]);
    const [message, setMessage] = useState(0);
    const [product, setProduct] = useState([]);



    useEffect(() => {
        const getOrder = async() => {
            await axios.get(`/api/get-user-order?page=${page}`)
                .then(res => setOrder(res))
                .catch(err => console.log(err))

            await axios.get(`/api/get-all-product`)
                .then(res => setProduct(res?.products?.rows))
                .catch(err => console.log(err))
        }

        getOrder();
    }, [message, detail, page]);


    const handleSetDetail = async(e) => {
        let target = e.target.closest('tr');
        if (!target) return;

        var valueOfTd = target.querySelector('td:first-child').innerText;

        
        await axios.get(`/api/search-order/${valueOfTd}`)
            .then(res => setData(res))
            .catch(err => console.log(err))

        await axios.get(`/api/get-all-status`)
            .then(res => setStatus(res))
            .catch(err => console.log(err))

        setDetail(true);
    }

    console.log(data);

  
    const handleEdit = async(value, id, unit, productId) => {
        const data = {
            order_status: value
        }

            const findProduct = product.filter(item => item.id === productId);

            if (value === '4') {
            let quantity = findProduct[0]?.total_products;
            const setQuantity = quantity + unit;

            const data = {
                total_products: setQuantity
            }

            await axios.put(`/api/edit-product/${productId}`, data)
                .then(res => console.log(res))
                .catch(err => console.log(err))

           
        } 

        await axios.put(`/api/edit-orderDetail/${id}`, data)
            .then(res => setMessage(res?.errCode))
            .catch(err => console.log(err))


        if (message === 0) {
            toast.success("Cập nhật trạng thái thành công!", {
                position: 'top-center'
            });
        } else {
            toast.error("Có lỗi xảy ra, vui lòng thử lại!", {
                position: 'top-center'
            });
        }

        setDetail(false);
    }

    return (
        <>
            <OrderComponent
                data={order}
                orderDetail={handleSetDetail}
                getValue={(e) => setPage(e.target.textContent)}
            />

            {detail && <Detail
                show={detail}
                handleClose={() => setDetail(false)}
                data={data}
                editDetail={handleEdit}
                status={status}
            />}
        </>
    );
}

export default OrderControl;