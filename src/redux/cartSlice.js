import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";


const cartSlice = createSlice({ 
    name: 'cart',
    initialState: {
        cartItems: localStorage.getItem("cartItems") ? JSON.parse(localStorage.getItem("cartItems")) : [],
        cartTotalQuantity: 0,
        cartTotalAmount: 0,
    },
    reducers: {
        addToCart(state, action) {
            const itemIndex = state?.cartItems?.findIndex(item => item.id === action?.payload?.id);

            if (itemIndex >= 0) {
                state.cartItems[itemIndex].cartQuantity += 1;
                toast.info(`Đã tăng số lượng sản phẩm ${state.cartItems[itemIndex].name} trong giỏ hàng!`, {
                    position: "top-center"
                });
            } else {
                const tempProduct = {...action?.payload, cartQuantity: 1}
                state.cartItems?.push(tempProduct);
                toast.success(`Thêm sản phẩm ${action.payload.name} vào giỏ hàng thành công!`, {
                    position: "top-center"
                });
            }

            localStorage.setItem("cartItems", JSON.stringify(state.cartItems));

        },
        removeFromCart(state, action) {
            const nextCartItem = state.cartItems.filter(item => item.id !== action.payload.id);

            state.cartItems = nextCartItem;

            localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
            toast.success(`Xóa sản phẩm ${action.payload.name} thành công!`, {
                position: "top-center"
            });
        },
        decreaseCart(state, action) {
            const itemIndex = state.cartItems.findIndex(item => item.id === action.payload.id);

            if (state.cartItems[itemIndex].cartQuantity > 1) {
                state.cartItems[itemIndex].cartQuantity -= 1;
            } else if (state.cartItems[itemIndex].cartQuantity === 1) {
                const nextCartItems = state.cartItems.filter(item => item.id !== action.payload.id);

                state.cartItems = nextCartItems;

                toast.success(`Xóa sản phẩm ${action.payload.name} thành công!`, {
                    position: "top-right"
                });
            }

            localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
        },
        clearCart(state, action) {
            state.cartItems = [];
            toast.success(`Đã xóa toàn bộ sản phẩm trong giỏ hàng thành công!`, {
                position: "top-right"
            });

            localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
        },
        getTotals(state, action) {
            let { total, quantity } = state.cartItems.reduce((cartTotal, cartItem) => {
                const { price, cartQuantity, sale } = cartItem;
                const itemTotal = sale ? ((price - sale * price / 100) * cartQuantity) : price * cartQuantity;

                cartTotal.total += itemTotal;
                cartTotal.quantity += cartQuantity;

                return cartTotal;
            }, {
                total: 0,
                quantity: 0
            })

            state.cartTotalQuantity = quantity;
            state.cartTotalAmount = total;
        },
        completedOrder(state, action) {
            state.cartItems = [];
            toast.success(`Bạn đã đặt hàng thành công!`, {
                position: "top-center"
            });

            localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
        },
        buyNow(state, action) {
            const itemIndex = state?.cartItems?.findIndex(item => item.id === action?.payload?.id);


            if (itemIndex >= 0) {
                state.cartItems[itemIndex].cartQuantity += action?.payload?.value;
                toast.info(`Đã tăng số lượng sản phẩm ${state.cartItems[itemIndex].name} trong giỏ hàng!`, {
                    position: "top-right"
                });
            } else {
                const tempProduct = {...action.payload, cartQuantity: action?.payload?.value}
                state.cartItems?.push(tempProduct);
                toast.success(`Thêm sản phẩm ${action.payload.name} vào giỏ hàng thành công!`, {
                    position: "top-center"
                });
            }

            localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
        },
        searchStore(state, action) {
            
        }
    }
})

export const {
    addToCart,
    removeFromCart,
    decreaseCart,
    clearCart,
    getTotals,
    completedOrder,
    buyNow
} = cartSlice.actions;

export default cartSlice.reducer;