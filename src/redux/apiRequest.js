import axios from "axios";
import { toast } from "react-toastify";
import { loginFailed, loginStart, loginSuccess, logOutStart, registerFailed, registerStart, registerSuccess, logOutFailed, logOutSuccess, changeData } from "./authSlice";
import { addToCart, removeFromCart, decreaseCart, clearCart, getTotals, completedOrder, buyNow } from "./cartSlice";

export const loginUser = async(user, dispatch, navigate) => {
    dispatch(loginStart());

    console.log(user);

    try {
        const res = await axios.post("/api/login", user); 
        if (res.errCode === 0) {
            dispatch(loginSuccess(res));
            navigate("/");
        } else {
            toast.error(`Tài khoản hoặc mật khẩu sai, vui lòng thử lại!`, {
                position: "top-center"
            });
            dispatch(loginFailed());
        }
    } catch (error) {
        dispatch(loginFailed());
    }
}



export const registerUser = async(user, dispatch, navigate) => {
    dispatch(registerStart());

    try {
        const res = await axios.post("/api/create-new-user", user);
        console.log(res);
        if (res.errCode === 0)  {
            dispatch(registerSuccess());
            toast.success(`Đăng kí tài khoản thành công!`, {
                position: "top-center"
            });
        } else {
            toast.error(`${res.errMessage}!`, {
                position: "top-center"
            });
            dispatch(registerFailed());
        }

    } catch (error) {
        dispatch(registerFailed());
    }
}

export const logOut = async(dispatch, navigate, accessToken) => {
    dispatch(logOutStart());
    try {
        await axios.get("/api/logout", {
            headers: {
                "token": `Bearer ${accessToken}`,
            }
        });

        dispatch(logOutSuccess());
        navigate("/");
    } catch (error) {
        dispatch(logOutFailed());
        console.log(error);
    }
}

export const addProductToCart = async(dispatch, product) => {
    dispatch(addToCart(product));
    // try {
    //     const data = await axios.get("/api/get-all-product");
    //     // console.log(data);

    //     if (data) {
    //         for (let i = 0; i < data.length; i++) {
    //             if (data[i].total_products >= product.cartQuantity) {
    //                 dispatch(addToCart(product));
    //             } else {
    //                 toast.error(`Số lượng sản phẩm không đủ!`, {
    //                     position: "top-center"
    //                 });
    //             }
    //         }
    //     }
    // } catch (error) {
    //     // dispatch(logOutFailed());
    //     console.log(error);
    // }
    
}

export const removeProductFromCart = (dispatch, product) => {
    dispatch(removeFromCart(product));
}

export const decreaseValue = (dispatch, product) => {
    dispatch(decreaseCart(product));
}

export const deleteCart = (dispatch) => {
    dispatch(clearCart());
}

export const getTotalMoney = (dispatch) => {
    dispatch(getTotals());
}

export const orderSuccess = (dispatch) => {
    dispatch(completedOrder());
}

export const buyProductNow = (dispatch, product) => {
    dispatch(buyNow(product));
}