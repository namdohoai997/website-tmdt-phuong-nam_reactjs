import BannerControl from "./BannerControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default BannerControl;