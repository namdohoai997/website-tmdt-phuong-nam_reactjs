
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./BannerControl.scss";

function BannerControl({
    banner = [],
    openEdit = () => {},
    getValue = () => {},
    removeCate = () => {},
    openAdd = () => {}
}) {

    console.log(banner);
    const errCode = banner?.errCode;

    const [keyword, setKeyword] = useState('');

    const panigate = []

    for (let i = 1; i <= banner?.total_page; i++) {
        if (!panigate.includes(i)) {
            panigate.push(i);
        }
    }



    const bannerList = banner?.Banners?.banners;
    if (errCode === 0) {
        return (
        
            <Container>
                <Row>
                    <Col>
                        <Row className="add-btn">
                            <Col>
                                <Form>
                                    <Form.Group className="mb-3" controlId="search">
                                        <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                    </Form.Group>
                                </Form>
    
                            </Col>
    
                            <Col>
                                <Button  onClick={openAdd}>Thêm danh mục</Button>
                            </Col>
                        </Row>
                        <h4 className="cate">Quản lí banner</h4>

                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Mã banner</th>
                                    <th>Ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Mô tả</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                    {bannerList?.map(banner => {
                                        if (keyword === "") {
                                            return (
                                                <tr key={banner.id}>
                                                    <td>{banner.id}</td>
                                                    <td><img width={90} src={banner.image}/></td>
                                                    <td>{banner.title}</td>
                                                    <td>{banner.description}</td>
                                                    <td>{banner.is_active === 1 ? 'Hiển thị' : 'Ẩn'}</td>
                                                    <th>
                                                        <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                        <Button className="remove-btn" variant="danger" onClick={removeCate}>Xóa</Button>
                                                    </th>
                                                </tr>
                                            )
                                        } else if (banner.title.includes(keyword.toLocaleLowerCase())) {
                                            return (
                                                <tr key={banner.id}>
                                                    <td>{banner.id}</td>
                                                    <td><img width={90} src={banner.image}/></td>
                                                    <td>{banner.title}</td>
                                                    <td>{banner.description}</td>
                                                    <td>{banner.is_active === 1 ? 'Hiển thị' : 'Ẩn'}</td>
                                                    <th>
                                                        <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                        <Button className="remove-btn" variant="danger" onClick={removeCate}>Xóa</Button>
                                                    </th>
                                                </tr>
                                            );
                                        }
                                    })
                         
                                }
                            </tbody>
    
                            <tfoot>
                                <Pagination onClick={getValue} className="d-flex justify-content-center">
                                    { panigate.map(item => (
                                        <Pagination.Item>{item}</Pagination.Item>
                                    )) }
                                </Pagination>
                            </tfoot>
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    } else {
        return <h3>Có lỗi xảy ra</h3>
    }
}

export default BannerControl;