import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Edit({
    show = false,
    data = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    console.log(data);
    const errCode = data?.errCode;
    const banners = data?.Banner;

    const [active, setActive] = useState(banners?.is_active);
    const [title, setTitle] = useState(banners?.title);
    const [image, setImage] = useState(banners?.image);
    const [description, setDescription] = useState(banners?.description);

    console.log(active);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
            {errCode === 0 ? (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin Banner</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                        <Form.Group controlId="id">
                                <Form.Control type="hidden" value={banners?.id} />
                                </Form.Group>
                                <Form.Group controlId="image">
                                <Form.Label>Ảnh</Form.Label>
                                
                                <Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} />
                                <img width={200} src={image}/>
                                </Form.Group>

                                <Form.Group controlId="title">
                                <Form.Label>Tiêu đề</Form.Label>
                                <Form.Control type="text" value={title} onChange={e => setTitle(e.target.value)} />
                                </Form.Group>

                                <Form.Group controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <Form.Control type="tex" value={description} onChange={e => setDescription(e.target.value)} />
                                </Form.Group>

                                <Form.Group as={Col} controlId="active">
                                    <Form.Label>Trạng thái</Form.Label>
                                    <Form.Select value={active} onChange={e => setActive(e.target.value)}>
                                        <option value={1} selected={active === 1 ? 'selected' : ''}>Hiển thị</option>
                                        <option value={0} selected={active === 0 ? 'selected' : ''}>Ẩn</option>
                                    </Form.Select>
                                </Form.Group>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={sendData}>Xác nhận</Button>
                    </Modal.Footer>
                </>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Đã có lỗi xảy ra, vui lòng thử lại
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Xác nhận
                        </Button>
                    </Modal.Footer>
                </>
            )}
        </Modal>
    );
}

export default Edit;