import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Add({
    show = false,
    data = [],
    category = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    // const errCode = data?.errCode || category?.errCode;
    // const brand = data?.brand;
    // const categories = category.cates;
    // const brands = brand?.brands.rows;

    const [active, setActive] = useState();
    const [title, setTitle] = useState();
    const [image, setImage] = useState();
    const [description, setDescription] = useState();

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
                    <Modal.Header closeButton>
                        <Modal.Title>Thêm danh mục</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form>
                                <Form.Group controlId="image">
                                <Form.Label>Ảnh</Form.Label>
                                
                                <Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} />
                                <img width={200} src={image}/>
                                </Form.Group>

                                <Form.Group controlId="title">
                                <Form.Label>Tiêu đề</Form.Label>
                                <Form.Control type="text" value={title} onChange={e => setTitle(e.target.value)} />
                                </Form.Group>

                                <Form.Group controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <Form.Control type="tex" value={description} onChange={e => setDescription(e.target.value)} />
                                </Form.Group>

                                <Form.Group as={Col} controlId="active">
                                    <Form.Label>Trạng thái</Form.Label>
                                    <Form.Select value={active} onChange={e => setActive(e.target.value)}>
                                        <option value={1} selected={active === 1 ? 'selected' : ''}>Hiển thị</option>
                                        <option value={0} selected={active === 0 ? 'selected' : ''}>Ẩn</option>
                                    </Form.Select>
                                </Form.Group>
                            </Form>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Đóng
                        </Button>
                        <Button variant="primary" onClick={sendData}>Thêm</Button>
                    </Modal.Footer>
              
            
        </Modal>
    );
}

export default Add;