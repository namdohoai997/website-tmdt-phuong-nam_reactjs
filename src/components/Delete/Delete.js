import { Modal, Button } from "react-bootstrap";

function Delete({
    show = false,
    handleClose = () => {},
    handleConfirm = () => {}
}) {
    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>Thông Báo</Modal.Title>
            </Modal.Header>
            <Modal.Body>Bạn có muốn xóa trường này không?</Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                Hủy
            </Button>
            <Button variant="primary" onClick={handleConfirm}>
                Xác nhận
            </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default Delete;