import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Edit({
    show = false,
    data = [],
    roles = [],
    brand = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    const errCode = data?.errCode || roles?.errCode;
    const user = data?.user;
    const roleData = roles?.roles?.rows;

    const [fname, setFName] = useState(user?.firstName);
    const [image, setImage] = useState(user?.image);
    const [lname, setLName] = useState(user?.lastName);
    const [gender, setGender] = useState(user?.gender);
    const [email, setEmail] = useState(user?.email);
    const [phone, setPhone] = useState(user?.phoneNumber);
    const [role, setRole] = useState(user?.roleId);
    const [address, setAddress] = useState(user?.address);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
            {errCode === 0 ? (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin tài khoản</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId="id">
                                <Form.Control type="hidden" value={user?.id} />
                                </Form.Group>
                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="lname">
                                <Form.Label>Họ</Form.Label>
                                <Form.Control type="text" value={lname} onChange={e => setLName(e.target.value)} />
                                </Form.Group>

                                <Form.Group as={Col} className="mb-3" controlId="fname">
                                <Form.Label>Tên</Form.Label>
                                <Form.Control type="text" value={fname} onChange={e => setFName(e.target.value)} />
                            </Form.Group>
                                
                            </Row>

                            <Form.Group controlId="image">
                                <Form.Label>Ảnh</Form.Label>
                                
                                <Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} />
                                <img width={200} src={image}/>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="address">
                                <Form.Label>Địa chỉ</Form.Label>
                                <Form.Control type="text" value={address} onChange={e => setAddress(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="phone">
                                <Form.Label>Số điện thoại</Form.Label>
                                <Form.Control type="number" value={phone} onChange={e => setPhone(e.target.value)} />
                            </Form.Group>

                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="gender">
                                    <Form.Label>Giới tính</Form.Label>
                                    <Form.Select value={gender} onChange={e => setGender(e.target.value)}>
                                        <option value={1} selected={gender === 1 ? 'selected' : ''}>Nam</option>
                                        <option value={0} selected={gender === 0 ? 'selected' : ''}>Nữ</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="roleId">
                                    <Form.Label>Quyền truy cập</Form.Label>
                                    <Form.Select value={role} onChange={e => setRole(e.target.value)}>
                                        {roleData?.map(item => (
                                            <option value={item.id} key={item.id} selected={role === item.id ? 'selected' : ''}>{item.name}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Row>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={sendData}>Xác nhận</Button>
                    </Modal.Footer>
                </>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Đã có lỗi xảy ra, vui lòng thử lại
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Xác nhận
                        </Button>
                    </Modal.Footer>
                </> 
            )}
        </Modal>
    );
}

export default Edit;