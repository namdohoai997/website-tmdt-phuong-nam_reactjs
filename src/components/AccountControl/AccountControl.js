
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./AccountControl.scss";

function AccountControl({
    users = [],
    others = [],
    openEdit = () => {},
    getValue = () => {},
    removeItem = () => {},
    openAdd = () => {},
}) {

    const [keyword, setKeyword] = useState('');
    const [value, setValue] = useState('');

    const panigate = []

    for (let i = 1; i <= users?.total_page; i++) {
        if (!panigate.includes(i)) {
            panigate.push(i);
        }
    }


    // if ()

    const user = users?.user?.users;
    const mod = others?.user?.users;
    return (
        <Container>
            <Row>
                <Col>
                    <Row className="add-btn">
                        <Col>
                            <Form>
                                <Form.Group className="mb-3" controlId="search">
                                    <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                </Form.Group>
                            </Form>

                        </Col>

                        <Col>
                            <Button  onClick={openAdd}>Thêm tài khoản</Button>
                        </Col>
                    </Row>
                    <h4 className="cate">Quản lí tài khoản</h4>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Mã khách hàng</th>
                                <th>Tên khách hàng</th>
                                <th>Ảnh</th>
                                <th>Địa chỉ</th>
                                <th>Giới tính</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Quyền</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                                {user?.map(item => {
                                    if (keyword === "") {
                                        return (
                                            <tr key={item.id}>
                                                <td>{item.id}</td>
                                                <td>{item.firstName + item.lastName}</td>
                                                <td><img width={90} className="img-item" src={item.image} alt={item.userName}/></td>
                                                <td>{item.address}</td>
                                                <th>{item.gender === 1 ? 'Nam' : 'Nữ'}</th>
                                                <th>{item.email}</th>
                                                <th>{item.phoneNumber}</th>
                                                <th>{item.role_name}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeItem}>Xóa</Button>
                                                </th>
                                            </tr>
                                        )
                                    } else if (item.email.includes(keyword.toLocaleLowerCase())) {
                                        return (
                                            <tr key={item.id}>
                                                <td>{item.id}</td>
                                                <td>{item.firstName + item.lastName}</td>
                                                <td><img width={90} className="img-item" src={item.image} alt={item.userName}/></td>
                                                <td>{item.address}</td>
                                                <th>{item.gender === 1 ? 'Nam' : 'Nữ'}</th>
                                                <th>{item.email}</th>
                                                <th>{item.role_name}</th>
                                                <th>{item.description}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeItem}>Xóa</Button>
                                                </th>
                                            </tr>
                                        );
                                    }
                                })
                    
                            }
                        </tbody>

                        <tfoot>
                            <Pagination onClick={getValue} className="d-flex justify-content-center">
                                { panigate.map(item => (
                                    <Pagination.Item>{item}</Pagination.Item>
                                )) }
                            </Pagination>
                        </tfoot>
                    </Table>
                </Col>
            </Row>

            <Row>
                <Col>
                    <Row className="add-btn">
                        <Col>
                            <Form>
                                <Form.Group className="mb-3" controlId="search">
                                    <Form.Control type="text" placeholder="Search" onChange={e => setValue(e.target.value)} />
                                </Form.Group>
                            </Form>

                        </Col>

                        {/* <Col>
                            <Button  onClick={openAdd}>Thêm tài khoản</Button>
                        </Col> */}
                    </Row>
                
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Mã khách hàng</th>
                                <th>Tên khách hàng</th>
                                <th>Ảnh</th>
                                <th>Địa chỉ</th>
                                <th>Giới tính</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Quyền</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                                {mod?.map(item => {
                                    if (value === "") {
                                        return (
                                            <tr key={item.id}>
                                                <td>{item.id}</td>
                                                <td>{item.firstName + item.lastName}</td>
                                                <td><img width={90} className="img-item" src={item.image} alt={item.userName}/></td>
                                                <td>{item.address}</td>
                                                <th>{item.gender === 1 ? 'Nam' : 'Nữ'}</th>
                                                <th>{item.email}</th>
                                                <th>{item.phoneNumber}</th>
                                                <th>{item.role_name}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeItem}>Xóa</Button>
                                                </th>
                                            </tr>
                                        )
                                    } else if (item.email.includes(value.toLocaleLowerCase())) {
                                        return (
                                            <tr key={item.id}>
                                                <td>{item.id}</td>
                                                <td>{item.firstName + item.lastName}</td>
                                                <td><img width={90} className="img-item" src={item.image} alt={item.userName}/></td>
                                                <td>{item.address}</td>
                                                <th>{item.gender === 1 ? 'Nam' : 'Nữ'}</th>
                                                <th>{item.email}</th>
                                                <th>{item.role_name}</th>
                                                <th>{item.description}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeItem}>Xóa</Button>
                                                </th>
                                            </tr>
                                        );
                                    }
                                })
                    
                            }
                        </tbody>

                        <tfoot>
                            <Pagination onClick={getValue} className="d-flex justify-content-center">
                                { panigate.map(item => (
                                    <Pagination.Item>{item}</Pagination.Item>
                                )) }
                            </Pagination>
                        </tfoot>
                    </Table>
                </Col>
            </Row>
        </Container>
    );
}

export default AccountControl;