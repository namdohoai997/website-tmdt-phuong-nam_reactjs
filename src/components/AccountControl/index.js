import AccountControl from "./AccountControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default AccountControl;