import { Col, Container, Row, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import 'react-multi-carousel/lib/styles.css';

import "./ProductDetail.scss";
import InputNumber from "../../InputNumber/InputNumber";
import LikeAndShare from "../../containers/Socials/LikeAndShare";
import Comment from "../../containers/Socials/Comment";
import ExtendProduct from "../ExtendProduct/ExtendProduct";
import Products from "../Products";
import { useEffect, useRef } from "react";

function ProductDetail({ 
    data = [],
    value = 1,
    isLogin = false,
    products = [],
    responsive = {},
    convertMoney = () => {},
    increase = () => {},
    decrease = () => {},
    buyNow = () => {},
}) {
    const product = data?.product?.rows[0];
    const inputRef = useRef();

    const productsBrand = products || [];

    console.log(value);

    return (
        <Container className="product-detail">
            <Row className="product-area">
                <Col className="col-5">
                    <div className="img-box">
                        <img src={product?.image} alt={product?.name} className="img-product"/>
                    </div>
                </Col>

                <Col className="col-7 d-flex flex-column">
                    { product?.is_hot === 1 ? <span className="hot-product">HOT</span> : '' }
                    <p className="product-name">{product?.name}</p>

                    <div className="product-price d-flex align-items-center">
                        {product?.sale ? (
                            product?.price && (
                                <h4 className="d-flex align-items-center">
                                    <p className="old-price">{convertMoney(product.price)}</p>
                                    <p className="parcel">-</p>
                                    <p className="sale-price">{convertMoney(product.price - product.price * product.sale / 100)}</p>
                                </h4>
                            )
                        ) : (
                            <p className="sale-price">{product?.price && convertMoney(product?.price)}</p>
                        )}

                        {product?.sale ? <span className="sale-value">{product?.sale}% giảm</span> : ''}
                    </div>
                    
                    {product?.total_products > 0 ? (
                        <>
                            <div className="input-product-buy">
                                <span>Số lượng</span>
                                <InputNumber
                                    decrease={value >= 1 ? decrease : () => {}}
                                    increase={value < product?.total_products ? increase : () => {}}
                                    value={value === 1 ? 1 : value}
                                    maxProduct={product?.total_products}
                                    minProduct={1}
                                />
                            </div>
                            <p className="product-quantity">{product?.total_products > 0 ? `${product?.total_products} sản phẩm có sẵn` : 'Hết hàng' }</p>

                            {value !== 0 ? (<Link to={isLogin && "/cart"}>
                                <Button onClick={() => buyNow(product)} className="buyNow">Mua Ngay</Button>
                            </Link>) : (
                                <h6 className="out-of-order">Bạn cần chọn số lượng</h6>
                            )}
                        </>
                    ) : (
                        <h6 className="out-of-order">Sản phẩm đã hết hàng</h6>
                    )}

                    <LikeAndShare />
                </Col>
                {/* <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=5011107445670930" width="450" height="35" className="likeBtn" scrolling="no" frameBorder="0" allowFullScreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe> */}
                
            </Row>

            <Row>
                <Col className="text-center">
                    <h5 className="descripiton">Mô tả sản phẩm</h5>
                    <ExtendProduct
                        data={product?.description}
                    />
                </Col>
            </Row>

            <Row className="options-box">
                <Col className="d-flex justify-content-center">
                    <Comment/>
                </Col>
            </Row>

            <Row className="others-product">
                <Products
                    data={productsBrand}
                    responsive={responsive}
                    id={product?.id}
                />
            </Row>
        </Container>
    );
}

export default ProductDetail;