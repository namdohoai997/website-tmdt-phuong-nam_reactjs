import { Container, Row, Col, Navbar, Offcanvas, Button, Form, FormControl, NavDropdown, Nav } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

import { useEffect, useState } from "react";
import { Link, Routes, } from "react-router-dom";

import "./Admin.scss";
// import Statistical from "../Statistical";
import ProductControl from "../ProductControl";

function Admin({
    role = false,
    backUser = () => {}
}) {
    

    return (
        // <Container fluid="auto">
            <Container >
                {/* <Row>
                    <Col>
                        <FontAwesomeIcon icon={faBars}/>
                    </Col>
                </Row> */}
                <Navbar bg="light" expand={false}>
                    <Container fluid>
                        <Navbar.Brand href="#"><Link to="/admin/statistical">Trang chủ</Link></Navbar.Brand>
                        <Navbar.Toggle aria-controls="offcanvasNavbar" />
                        <Navbar.Offcanvas
                        id="offcanvasNavbar"
                        aria-labelledby="offcanvasNavbarLabel"
                        placement="end"
                        >
                        <Offcanvas.Header closeButton>
                            <Offcanvas.Title id="offcanvasNavbarLabel">Menu</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <Nav className="justify-content-end flex-grow-1 pe-3">
                            <Link className="linkAdmin" to="/admin/statistical">Trang chủ</Link>
                            <Link className="linkAdmin" to="/admin/product">Quản lí sản phẩm</Link>
                            <Link className="linkAdmin" to="/admin/order">Quản lí đơn hàng</Link>
                            {role && <Link className="linkAdmin" to="/admin/account">Quản lí tài khoản</Link>}
                            {role && <Link className="linkAdmin" to="/admin/role">Quản lí quyền</Link>}
                            <Link className="linkAdmin" to="/admin/brand">Quản lí thương hiệu</Link>
                            <Link className="linkAdmin" to="/admin/category">Quản lí danh mục</Link>
                            <Link className="linkAdmin" to="/admin/banner">Quản lí Banner</Link>
                            {role && <Link className="linkAdmin" to="/admin/transport">Quản lí giao hàng</Link>}
                            {role && <Link className="linkAdmin" to="/admin/setting">Cấu hình website</Link>}
                            {/* <Link to="/" onClick={backUser}>Quay về màn hình User</Link> */}
                            </Nav>
                        </Offcanvas.Body>
                        </Navbar.Offcanvas>
                        </Container>
                    </Navbar>

            
             </Container>
        // </Container>
    );
}

export default Admin;