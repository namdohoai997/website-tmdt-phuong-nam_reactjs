import { Col, Container, Row, ListGroup } from "react-bootstrap";

import "./Footer.scss";

function Footer({ setting }) {
    return (
        <Container fluid="auto" className="footer">
            <Container>
                <Row>
                    <Col className="col-3">
                        <ListGroup variant="flush" className="info">
                            <ListGroup.Item className="info-item">{setting[0]?.name}</ListGroup.Item>
                            <ListGroup.Item className="info-item">Email: {setting[0]?.email}</ListGroup.Item>
                            <ListGroup.Item className="info-item">SĐT: {setting[0]?.phone}</ListGroup.Item>
                            <ListGroup.Item className="info-item">Fax: {setting[0]?.fax}</ListGroup.Item>
                        </ListGroup>
                    </Col>

                    <Col className="col6">
                        {setting[0]?.description}
                    </Col>

                    <Col className="col-3">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FPN-E-commerce-112299444794404&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=784476549193440" width="340" height="300" className="fanpage" scrolling="no" frameBorder="0" allowFullScreen={true} allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    </Col>
                </Row>
            </Container>
        </Container>
    );
}

export default Footer;