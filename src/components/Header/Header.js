import { Row, Col, Container, Form, Button, Dropdown } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faUser, faSearch, faCartShopping, faArrowRightToBracket, faRegistered } from '@fortawesome/free-solid-svg-icons';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    useLocation 
} from "react-router-dom";
import { Fragment, useEffect, useRef, useState } from "react";
import { toast } from "react-toastify";


import "./Header.scss";
import logo2 from "../../assets/logo/logo2.png";
import UserContext from "../../context/UserContext";
import { NotFound } from "../Cart";

function Header({
    user = null,
    cart = null,
    search = '',
    setting = [],
    logout = () => {},
    searchItem = () => {},
    accountForm = () => {},
    getInfo = () => {},
    getCart = () => {},
    adminPage = () => {}
}) { 

    const phone = setting[0]?.phone;
    const logo = setting[0]?.logo;
    const data = user?.user;
    const location = useLocation();
    const [keyword, setKeyword] = useState('');
    const totalQuantity = cart?.cartTotalQuantity;
    
    return (
            <Container fluid="auto" className="header">
                <Container fluid="lg">
                    <Row>
                        <Col>
                            <h6 className="phone">Phone: {phone}</h6>
                        </Col>

                        <Col className="d-flex justify-content-end">
                            {data ? (
                                <Fragment>
                                    <Link to='/noti' className="link notifi">
                                        <FontAwesomeIcon icon={faBell} className="icon"/>
                                        Thông báo
                                    </Link>
                                    
                                    <Dropdown>
                                        <Dropdown.Toggle id="dropdown-basic">
                                            Xin chào, {data.userName}
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item onClick={getInfo}>Thông tin tài khoản</Dropdown.Item>
                                            <Dropdown.Item onClick={getCart}>Giỏ hàng của bạn</Dropdown.Item>
                                            {data.roleId !== 3 ? (<Dropdown.Item><Link to="/admin/statistical" onClick={adminPage}>Trang ADMIN</Link></Dropdown.Item>) : ''}
                                            <Dropdown.Item onClick={logout}>Đăng xuất</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Fragment>
                            ) : 
                            (
                                <Fragment>
                                    <Link to='/noti' className="link">
                                        <FontAwesomeIcon icon={faBell} className="icon"/>
                                        Thông báo
                                    </Link>

                                    <Button variant="link" className="link" onClick={accountForm}>
                                        <FontAwesomeIcon icon={faUser} className="icon"/>
                                        Tài Khoản
                                    </Button>
                                </Fragment>
                            )}
                        </Col>
                    </Row>

                    {/* {header && ( */}
                        <Row className="header-bar-search">
                            <Col className="d-flex col-3 align-items-center">
                                <Link to="/">
                                    <img src={logo} width={100}/>
                                </Link>
                            </Col>

                            <Col className="d-flex col-6 align-items-center">
                                <Form className="d-flex form" method="GET" onSubmit={e => e.preventDefault()}>
                                    <Form.Group className="form-width" controlId="search">
                                        <Form.Control 
                                            type="search" 
                                            placeholder="Tìm kiếm" 
                                            value={keyword}
                                            onChange={e => setKeyword(e.target.value)}
                                        />
                                    </Form.Group>
                                        <Link to={`/search/${keyword}/`} className="link searchBtn" type="submit">
                                            <Button variant="primary" onClick={() => searchItem(keyword)} className="search">
                                                <FontAwesomeIcon icon={faSearch} className="icon-search" type="submit"/>
                                            </Button>
                                        </Link>
                                </Form>
                            </Col>
                            <Col className="d-flex col-3 align-items-center justify-content-end">
                                <Link to={data ? '/cart' : '/account'} className="link">
                                    <FontAwesomeIcon icon={faCartShopping} className="icon"/>
                                    Giỏ hàng

                                    <span className="total-product">{data ? (totalQuantity ? totalQuantity : 0) : 0}</span>
                                </Link>
                            </Col>
                        </Row>
                        {/* )}  */}
                </Container>
            </Container>
    );
}

export default Header;