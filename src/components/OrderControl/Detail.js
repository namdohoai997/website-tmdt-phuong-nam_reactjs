import { Modal, Button, Form, Col, Row, Table,  } from "react-bootstrap";
import { useState } from "react";

function Detail({
    show = false,
    data = [],
    status = [],
    handleClose = () => {},
    sendData = () => {},
    editDetail = () => {},
}) {
    const [update, setUpdate] = useState(null);
    console.log(data);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin đơn hàng</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Mã đơn</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Tổng tiền</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                    {data?.order?.map(order => (
                                        <tr key={order.id}>
                                            <td>{order.id}</td>
                                            <td>{order.productName}</td>
                                            <td>{order.quantity}</td>
                                            <th>{order.unit_price}</th>
                                            <th>{order.total_price}</th>
                                            <th>
                                                <select onChange={e => setUpdate(e.target.value)}>
                                                    {status?.status.map(item => (
                                                        <option key={item.id} value={item.id} selected={item.id === order.order_status ? 'selected' : ''}>{item.name}</option>
                                                    ))}
                                                </select>    
                                            </th>
                                            <th>
                                                <Button className="remove-btn" variant="primary" onClick={() => editDetail(update, order.id, order.quantity, order.product_id)}>Cập nhật trạng thái</Button>
                                            </th>
                                        </tr>
                                    ))  
                        
                                }
                            </tbody>
                        </Table>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Quay lại
                        </Button>
                        {/* <Button variant="primary" onClick={sendData()}>Thêm</Button> */}
                    </Modal.Footer>
              
            
        </Modal>
    );
}

export default Detail;