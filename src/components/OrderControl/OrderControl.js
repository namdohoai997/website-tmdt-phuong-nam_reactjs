
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./OrderControl.scss";

function OrderControl({
    data = [],
    orderDetail = () => {},
    getValue = () => {},
    removeOrder = () => {},
    openAdd = () => {},
}) {
    // console.log(data.total_page);
    const orders = data?.orders?.order;


    const [keyword, setKeyword] = useState('');

    const panigate = []

    for (let i = 1; i <= data?.total_page; i++) {
        if (!panigate.includes(i)) {
            panigate.push(i);
        }
    }


    if (data.errCode === 0) {
        return (
            <Container>
                    <Row>
                        <Col>
                            <Row className="add-btn">
                                <Col>
                                    <Form>
                                        <Form.Group className="mb-3" controlId="search">
                                            <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                        </Form.Group>
                                    </Form>

                                </Col>
                            </Row>
                            <h4 className="cate">Quản lí đơn hàng</h4>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Mã đơn hàng</th>
                                        <th>Người đặt hàng</th>
                                        <th>Tổng tiền</th>
                                        <th>Mô tả</th>
                                        <th>Ngày đặt</th>
                                        <th>Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        {orders?.map(order => {
                                            if (keyword === "") {
                                                return (
                                                    <tr key={order.id}>
                                                        <td>{order.id}</td>
                                                        <td>{order.userName}</td>
                                                        <td>{order.total_price}</td>
                                                        <th>{order.description}</th>
                                                        <th>{order.createdAt}</th>
                                                        <th>
                                                            <Button className="remove-btn" variant="primary" onClick={orderDetail}>Xem chi tiết</Button>
                                                        </th>
                                                    </tr>
                                                )
                                            } else if (order.id.includes(keyword.toLocaleLowerCase())) {
                                                return (
                                                    <tr key={order.id}>
                                                        <td>{order.id}</td>
                                                        <td>{order.userName}</td>
                                                        <td>{order.total_price}</td>
                                                        <th>{order.description}</th>
                                                        <th>{order.createdAt}</th>
                                                        <th>
                                                            <Button className="remove-btn" variant="primary" onClick={orderDetail}>Xem chi tiết</Button>
                                                        </th>
                                                    </tr>
                                                );
                                            }
                                        }) 
                            
                                    }
                                </tbody>

                                <tfoot>
                                    <Pagination onClick={getValue} className="d-flex justify-content-center">
                                        { panigate.map(item => (
                                            <Pagination.Item>{item}</Pagination.Item>
                                        )) }
                                    </Pagination>
                                </tfoot>
                            </Table>
                        </Col>
                    </Row>
                </Container>
        )
    } else {
        <h4>Có lỗi xảy ra</h4>
    }
    
}

export default OrderControl;