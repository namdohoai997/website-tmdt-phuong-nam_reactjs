import { Modal, Form, Button } from "react-bootstrap";
import { useState } from "react";

function ChangInfo({
    data = {},
    show = false,
    selectedFile = null,
    preview = null,
    setChange = () => {},
    handleChange = () => {},
    setShow = () => {}
}) {

    const userInfo = data?.user;

    const [fname, setFname] = useState(userInfo?.firstName);
    const [lname, setLname] = useState(userInfo?.lastName);
    const [address, setAddress] = useState(userInfo?.address);
    const [gender, setGender] = useState(userInfo?.gender);
    const [password, setPassword] = useState(userInfo?.password);
    const [phone, setPhone] = useState(userInfo?.phoneNumber);
    const [avatar, setAvartar] = useState(userInfo?.image);
    
    console.log(userInfo?.gender);

    return (
        <Modal
          show={show}
          fullscreen={true}
          onHide={setShow}
          dialogClassName="modal-90w"
          aria-labelledby="example-custom-modal-styling-title"
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header className="d-flex justify-content-between">
            <Modal.Title id="example-custom-modal-styling-title"> 
              Đổi thông tin tài khoản
            </Modal.Title>
            <Button onClick={setShow}>Quay lại</Button>
          </Modal.Header>
          <Modal.Body>
          <Form>
                <Form.Group className="mb-3" controlId="id">
                    <Form.Control type="hidden" value={userInfo.id}  />
                </Form.Group>
              <Form.Group className="mb-3" controlId="fname">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control type="text" value={fname} onChange={e => setFname(e.target.value)} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="lname">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control type="text"  value={lname} onChange={e => setLname(e.target.value)}  />
              </Form.Group>
              <Form.Group className="mb-3" controlId="address">
                  <Form.Label>Address</Form.Label>
                  <Form.Control type="text" placeholder="Address"  value={address} onChange={e => setAddress(e.target.value)}  />
              </Form.Group>
              <Form.Label>Giới tính</Form.Label>
              <Form.Select value={gender} onChange={e => setGender(e.target.value)} >
                  <option value={1} selected={gender === true ? 'selected' : ''}>Nam</option>
                  <option value={0} selected={gender === false ? 'selected' : ''}>Nữ</option>
              </Form.Select>
              <Form.Group className="mb-3" controlId="email">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type="email" value={userInfo.email} disabled />
              </Form.Group>
              
              <Form.Group className="mb-3" controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="pnumber">
                  <Form.Label>Phone Number</Form.Label>
                  <Form.Control type="number" placeholder="Phone Number"  value={phone} onChange={e => setPhone(e.target.value)}  />
              </Form.Group>
              <Form.Group controlId="avatar" className="mb-3">
                  <Form.Label>Avatar</Form.Label>
                  <Form.Control 
                      type="file" 
                      onChange={e => setAvartar(e.target.value)}
                  />
                  {selectedFile && <img src={preview} width={200}/>}
              </Form.Group>
              <Button variant="primary" onClick={(e) => handleChange(e, gender)}>
                  Xác nhận thông tin
              </Button>
              
          </Form>
          </Modal.Body>
        </Modal>
    );
}

export default ChangInfo;