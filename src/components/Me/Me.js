import { useState } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { Modal, Button, Form, ListGroup } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";

import { loginUser } from "../../redux/apiRequest";

function Me({
    data = {},
    show = false,
    setShow = () => {},
    setChange = () => {}
}) {
    
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const userInfo = data?.user;

    // const [errCode, setErrCode] = useState(0);
    // const [change, setChange] = useState(false);
    // const [fname, setFname] = useState(userInfo?.firstName);
    // const [lname, setLname] = useState(userInfo?.lastName);
    // const [address, setAddress] = useState(userInfo?.address);
    // const [gender, setGender] = useState(userInfo?.gender);
    // const [password, setPassword] = useState(userInfo?.password);
    // const [phone, setPhone] = useState(userInfo?.phoneNumber);
    // const [avatar, setAvartar] = useState(userInfo?.image);

    


  return (
        <Modal
            show={show}
            fullscreen={true}
            onHide={() => setShow(true)}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
            backdrop="static"
            keyboard={false}
        >
        <Modal.Header className="d-flex justify-content-between">
          <Modal.Title id="example-custom-modal-styling-title"> 
            Thông tin tài khoản
          </Modal.Title>
          <Button onClick={() => setShow(false)}>Close</Button>
        </Modal.Header>
        <Modal.Body>
            <ListGroup>
                <ListGroup.Item>Tên: {userInfo.firstName}</ListGroup.Item>
                <ListGroup.Item>Họ: {userInfo.lastName}</ListGroup.Item>
                <ListGroup.Item>Địa chỉ: {userInfo.address}</ListGroup.Item>
                <ListGroup.Item>Giới tính: {userInfo.gender === true ? 'Nam' : 'Nữ'}</ListGroup.Item>
                <ListGroup.Item>SĐT: {userInfo.phoneNumber}</ListGroup.Item>
                <ListGroup.Item>Email: {userInfo.email}</ListGroup.Item>
                <ListGroup.Item>Avatar: <img src={userInfo.image}/></ListGroup.Item>

            </ListGroup>
            <Button variant="link" onClick={setChange}>
                Đổi Thông Tin Tài Khoản
            </Button>
        </Modal.Body>
      </Modal>
  );
}

export default Me;