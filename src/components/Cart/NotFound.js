import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./Cart.scss"

function NotFound() {
    return (
        <Container>
            <Row>
                <Col>
                    <h3 className="not-found">Giỏ hàng của bạn hiện tại đang trống, click tại <Link to="/">Đây</Link> để về trang chủ!</h3>
                </Col>
            </Row>

            <Row className="d-flex justify-content-center">
                <Col className="col-8">
                    <div className="background-image"></div>
                </Col>
            </Row>
        </Container>
    );
}

export default NotFound;