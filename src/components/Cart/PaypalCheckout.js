import { PayPalScriptProvider, PayPalButtons } from "@paypal/react-paypal-js";
import { useState, useEffect } from "react";

<script src="https://www.paypal.com/sdk/js?client-id=YOUR_CLIENT_ID&currency=VND"></script>

function PaypalCheckout({ 
    data,
    approvePaypal = () => {}
}) {

    const price = data.cartTotalAmount * 0.000044;

    const [paidFor, setPaidFor] = useState(false);
    const [error, setError] = useState(null);

    

    // if (paidFor) {
    //     alert("Cảm ơn bạn đã mua hàng");
    // }

    if (error) {
        alert(error);
    }

    return (
        <PayPalScriptProvider>
            <PayPalButtons
                onClick={(data, actions) => {
                    const hasAlreadyBought = false;

                    if (hasAlreadyBought) {
                        setError("Bạn đã mua sản phẩm này rồi");
                        return actions.reject();
                    } else {
                        actions.resolve();
                    }
                }}
                createOrder = {(data, actions) => {
                    // data?.cartItems?.map(item => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    description: 'abc',
                                    amount: {
                                        currency_code: 'USD',
                                        value: price.toFixed(2)
                                    }
                                }
                            ]
                        })
                    // })
                    
                }}
                onApprove = {async(data, actions) => {
                    const order = await actions.order.capture();
                    // console.log("order", order);

                    approvePaypal(order);
                }}
                onCancel = {() => {

                }}
                onError = {(error) => {
                    setError(error);
                }}
            />
        </PayPalScriptProvider>
    )
}

export default PaypalCheckout;