import { Col, Container, Row, Table, Button, FloatingLabel, Form } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { useState } from "react";


import PaypalCheckout from "./PaypalCheckout";
import InputNumber from "../../InputNumber/InputNumber";
import NotFound from "./NotFound";

function Cart({ 
    data,
    maxProduct = 0,
    transport = [],
    transportPrice = 0,
    paypal = {},
    deleteProduct = () => {},
    convertMoneyFomat = () => {},
    channgeValue = () => {},
    decrease = () => {},
    increase = () => {},
    deleteCart = () => {},
    checkOut = () => {},
    description = () => {},
    transportId = () => {},
    approvePaypal = () => {}
}) {
    let cartLength = data.cartItems.length;
    const [value, setValue] = useState(null);
    const local = data;
    return (
        cartLength > 0 ? (
            <Container>
                <Row>
                    <Col>
                        <h3>Giỏ hàng của bạn</h3> 
                    </Col>
                </Row>

                <Row>
                    <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tổng tiền</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            { data.cartItems.map(item => {
                                let priceSale = item.price - item.sale * item.price / 100 ;

                                console.log(item.cartQuantity);

                                return (
                                    <tr key={item.id}>
                                        <td>
                                            <img src={item.image} width={160}/>
                                            <span>{item.name}</span>
                                        </td>
                                        <td>{item.sale ? convertMoneyFomat(priceSale) : convertMoneyFomat(item.price)}</td>
                                        <td>
                                            <InputNumber
                                                value={item.cartQuantity > item.total_products ? item.total_products : item.cartQuantity}
                                                decrease={() => decrease(item)}
                                                increase={() => increase(item)}
                                                channgeValue={channgeValue}
                                                maxProduct={item.total_products - 1}
                                            />
                                        </td>
                                        <td>{item.sale ? convertMoneyFomat(item.cartQuantity * priceSale) : convertMoneyFomat(item.cartQuantity * item.price)}</td>
                                        <td>
                                            <FontAwesomeIcon icon={faTrash} onClick={() => deleteProduct(item)}/>
                                        </td>
                                    </tr>
                                )
                                
                                }) }
                        </tbody>

                        <tfoot>
                            <tr>
                                <td colSpan={5}>
                                    <FloatingLabel controlId="floatingTextarea2" label="Mô tả">
                                        <Form.Control
                                            as="textarea"
                                            placeholder="Leave a comment here"
                                            style={{ height: '100px' }}
                                            onChange={description}
                                        />
                                    </FloatingLabel>
                                </td>
                            </tr>

                            <tr>
                                <th colSpan={2}>
                                    <Button variant="danger" onClick={deleteCart}>Xóa giỏ hàng</Button>
                                </th>

                                <th colSpan={3}>
                                    <Form.Label htmlFor="options">Phương thức vận chuyển</Form.Label>
                                    <Form.Select aria-label="Default select example" id="options" onChange={(e) => transportId(e.target.value)}>
                                        { transport?.rows?.map(item => (
                                            <option key={item.id} value={item.id}>{item.name} - {convertMoneyFomat(item.price)}</option>
                                        )) }
                                    </Form.Select>

                                    <p>Số tiền sản phẩm: {convertMoneyFomat(data.cartTotalAmount)}</p>
                                    <p>Số tiền thanh toán: {convertMoneyFomat(data.cartTotalAmount + transportPrice)}</p>
                                    
                                    <div className="d-flex justify-content-between">
                                        <Button className="btnOrder" variant="primary" onClick={() => checkOut()}>Đặt hàng</Button>
                                        <Button variant="success" >
                                            <PaypalCheckout 
                                                data={local}
                                                approvePaypal={approvePaypal} 
                                            />
                                        </Button>
                                    </div>
                                </th>
                            </tr>
                        </tfoot>
                        </Table>
                    </Col>
                </Row>
            </Container>
        ) : (
            <NotFound/>
        )
    )
}

export default Cart;