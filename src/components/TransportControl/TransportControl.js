
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./TransportControl.scss";

function TransportControl({
    transport = [],
    openEdit = () => {},
    removeItem = () => {},
    openAdd = () => {}
}) {

    const errCode = transport?.errCode;

    const [keyword, setKeyword] = useState('');


    const transports = transport?.Transports?.rows;
    if (errCode === 0) {
        return (
        
            <Container>
                <Row>
                    <Col>
                        <Row className="add-btn">
                            <Col>
                                <Form>
                                    <Form.Group className="mb-3" controlId="search">
                                        <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                    </Form.Group>
                                </Form>
    
                            </Col>
    
                            <Col>
                                <Button  onClick={openAdd}>Thêm danh mục</Button>
                            </Col>
                        </Row>
                        <h4 className="cate">Quản lí giao hàng</h4>
                    
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Mã banner</th>
                                    <th>Tên</th>
                                    <th>Cước phí</th>
                                    <th>Mô tả</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                    {transports?.map(transport => {
                                            return (
                                                <tr key={transport.id}>
                                                    <td>{transport.id}</td>
                                                    <td>{transport.name}</td>
                                                    <td>{transport.price}</td>
                                                    <td>{transport.description}</td>
                                                    <th>
                                                        <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                        <Button className="remove-btn" variant="danger" onClick={removeItem}>Xóa</Button>
                                                    </th>
                                                </tr>
                                            )
                                    }) 
                         
                                }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    } else {
        return <h3>Có lỗi xảy ra</h3>
    }
}

export default TransportControl;