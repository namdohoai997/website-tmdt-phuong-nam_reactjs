import CategoryControl from "./CategoryControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default CategoryControl;