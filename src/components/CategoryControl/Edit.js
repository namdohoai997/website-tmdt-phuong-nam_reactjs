import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Edit({
    show = false,
    data = [],
    handleClose = () => {},
    sendData = () => {}
}) {
    const errCode = data?.errCode;
    const categories = data?.category[0];

    const [name, setName] = useState(categories?.name);
    const [active, setActive] = useState(Number(categories?.is_active));

    console.log(active);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
            {errCode === 0 ? (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin sản phẩm</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId="id">
                                <Form.Control type="hidden" value={categories?.id} />
                                </Form.Group>
                                <Form.Group controlId="name">
                                <Form.Label>Tên thương hiệu</Form.Label>
                                <Form.Control type="name" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                            <Row className="mb-3">

                                <Form.Group as={Col} controlId="active">
                                    <Form.Label>Trạng thái</Form.Label>
                                    <Form.Select value={Number(active)} onChange={e => setActive(Number(e.target.value))}>
                                        <option value={1} selected={Number(active) === 1 ? 'selected' : ''}>Hiển thị</option>
                                        <option value={0} selected={Number(active) === 0 ? 'selected' : ''}>Ẩn</option>
                                    </Form.Select>
                                </Form.Group>
                            </Row>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={sendData}>Xác nhận</Button>
                    </Modal.Footer>
                </>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Đã có lỗi xảy ra, vui lòng thử lại
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Xác nhận
                        </Button>
                    </Modal.Footer>
                </>
            )}
        </Modal>
    );
}

export default Edit;