
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./CategoryControl.scss";

function CategoryControl({
    categories = [],
    openEdit = () => {},
    getValue = () => {},
    removeCate = () => {},
    openAdd = () => {}
}) {

    console.log(categories);
    const errCode = categories?.errCode;

    const [keyword, setKeyword] = useState('');


    const categoryList = categories?.cates;
    if (errCode === 0) {
        return (
        
            <Container>
                <Row>
                    <Col>
                        <Row className="add-btn">
                            <Col>
                                <Form>
                                    <Form.Group className="mb-3" controlId="search">
                                        <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                    </Form.Group>
                                </Form>
    
                            </Col>
    
                            <Col>
                                <Button  onClick={openAdd}>Thêm danh mục</Button>
                            </Col>
                        </Row>

                        <h4 className="cate">Quản lí danh mục</h4>
                    
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Mã danh mục</th>
                                    <th>Tên danh mục</th>
                                    <th>Trạng thái</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                    {categoryList?.map(cate => {
                                        if (keyword === "") {
                                            return (
                                                <tr key={cate.id}>
                                                    <td>{cate.id}</td>
                                                    <td>{cate.name}</td>
                                                    <td>{cate.is_active === 1 ? 'Hiển thị' : 'Ẩn'}</td>
                                                    <th>
                                                        <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                        <Button className="remove-btn" variant="danger" onClick={removeCate}>Xóa</Button>
                                                    </th>
                                                </tr>
                                            )
                                        } else if (cate.slug.includes(keyword.toLocaleLowerCase())) {
                                            return (
                                                <tr key={cate.id}>
                                                    <td>{cate.id}</td>
                                                    <td>{cate.name}</td>
                                                    <td>{cate.is_active === 1 ? 'Hiển thị' : 'Ẩn'}</td>
                                                    <th>
                                                        <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                        <Button className="remove-btn" variant="danger" onClick={removeCate}>Xóa</Button>
                                                    </th>
                                                </tr>
                                            );
                                        }
                                    })
                         
                                }
                            </tbody>
    
                            {/* <tfoot>
                                <Pagination onClick={getValue} className="d-flex justify-content-center">
                                    { panigate.map(item => (
                                        <Pagination.Item>{item}</Pagination.Item>
                                    )) }
                                </Pagination>
                            </tfoot> */}
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    } else {
        return <h3>Có lỗi xảy ra</h3>
    }
}

export default CategoryControl;