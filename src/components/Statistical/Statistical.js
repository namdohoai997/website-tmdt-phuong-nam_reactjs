import { Bar, Line } from "react-chartjs-2";
import { Chart as ChartJS } from "chart.js/auto";
import { Row, Col, Form, Table, Button } from "react-bootstrap";
import * as XLSX from 'xlsx';

import "./Statistical.scss";


function Statistical({
    month = [],
    products = [],
    count = [], 
    money = [],
    brand = [],
    product = 0,
    setMonth = () => {}
}) {

    const handleExport = (table) => {
        let wb = XLSX.utils.book_new();
        let ws = XLSX.utils.json_to_sheet(table);

        XLSX.utils.book_append_sheet(wb, ws, "MySheet1");

        XLSX.writeFile(wb, "MyExcel.xlsx");
    }

    const monthArr = [];
    function test() {
        month?.forEach(item => {
            if (!monthArr.includes(item.Month))
                monthArr.push(item.Month)
        })
        return monthArr
    }

    function test2(a) {
        return  month?.filter(i => i.Month === a).length;
    }

    test();

    return (
        <>
            <Row className="box-content">
                <Col className="col-8">
                    <h5 className="title text-center">Số lượng đơn hàng hàng tháng</h5>
                    <Bar
                    datasetIdKey='id'
                    data={{
                        labels: monthArr.map(item => `Tháng ${item}`),
                        datasets: [
                        {
                            label: '',
                            data: monthArr.map(item => test2(item)),
                            backgroundColor: [
                                'rgb(255, 99, 132)',
                                'rgb(54, 162, 235)',
                                'rgb(255, 205, 86)',
                            ],
                        }
                        ],
                    }}
                    />
                </Col>

                <Col className="col-2 value">
                    {monthArr.map(item => <p>Tháng {item}: {test2(item)} đơn hàng</p>)}
                </Col>
                <Col className="col-2 value">
                    <p>Số lượng thương hiệu: {brand.length}</p>
                    <p>Số lượng sản phẩm: {product}</p>
                </Col>
                <Button className="btn-width" onClick={() => handleExport(month)}>Export Excel</Button>
            </Row>

            <Row>
                <h5 className="d-flex justify-content-center text">Thống kê sản phẩm bán ra</h5>
                <Row className="row-product">

                    <Col className="col-3">
                        <Form.Select aria-label="Default select example" onChange={e => setMonth(e.target.value)}>
                        <option value="1">Tháng 1</option>
                        <option value="2">Tháng 2</option>
                        <option value="3">Tháng 3</option>
                        <option value="4">Tháng 4</option>
                        <option value="5">Tháng 5</option>
                        <option value="6">Tháng 6</option>
                        <option value="7">Tháng 7</option>
                        <option value="8">Tháng 8</option>
                        <option value="9">Tháng 9</option>
                        <option value="10">Tháng 10</option>
                        <option value="11">Tháng 11</option>
                        <option value="12">Tháng 12</option>
                        </Form.Select>
                    </Col>

                    
                </Row>

                <Row className="d-flex justify-content-center">
                    <Col className="col-10">
                        {products.length !== 0 ? 
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                <th>Tháng</th>
                                <th>Tên sản phẩm</th>
                                <th>Số lượng bán được</th>
                                <th>Tổng thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                {products?.map(product => (
                                    <tr>
                                        <td>{product.month}</td>
                                        <td>{product.name}</td>
                                        <td>{product.quantity}</td>
                                        <td>{Number(product.total).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
                                    </tr>
                                ))}
                            </tbody>
                            <Button onClick={() => handleExport(products)}>Export Excel</Button>
                        </Table> : 'Không có dữ liệu'}
                    </Col>
                </Row>
                
            </Row>

            <Row className="col-10">
                <h5 className="title text-center">Doanh thu hàng tháng</h5> 
                <Line
                    data={{
                        labels: money.map(item => `Tháng ${item.month}`),
                        datasets: [
                            {
                                label: 'Doanh thu hàng tháng',
                                data: money.map(item => item.total),
                                fill: false,
                                borderColor: 'rgb(75, 192, 192)',
                                tension: 0.1
                            
                            },
                        ],
                        
                    }}
                />
                <Button className="btn-width" onClick={() => handleExport(money)}>Export Excel</Button>
            </Row>
        </>
    );
}

export default Statistical;