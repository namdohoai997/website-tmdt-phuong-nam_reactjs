import { Col, Container, Row } from "react-bootstrap";

import Products, { NotFound } from "../Products";

import "./Search.scss";

function Search({ 
    data
}) {
    const productCount = data?.product?.count === 0;
    return (
        <Container>
            {productCount ? 
                <NotFound/> 
                : 
                <Products
                    data={data}
                />
            }

           
        </Container>
    );
}

export default Search;