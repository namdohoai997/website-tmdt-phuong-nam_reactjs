import { useEffect, useRef, useState } from "react";
import { Col, Container, Row, Form } from "react-bootstrap";
import axios from "axios";

import Products from "../Products";
import NotFound from "./Notfound";
import "./Filter.scss";

function FilterProducts({ 
    data = [],
    brands = []
}) {

    const [checked, setChecked] = useState([]);
    const [checkbox, setCheckbox] = useState(0);
    const [productFilter, setProductFilter] = useState([]);

    console.log(productFilter);

    let cate;

    const href = window.location.href;

    const path = href.substring(21, href.length)

    if (path === "/phone") {
        cate = 10;
    } else if (path === "/laptop") {
        cate = 9;
    } else if (path === '/accessory') {
        cate = 11
    }

    const [filter, setFilter] = useState({
        id: ['0'], 
        price: 0,
        cate: cate
    });

   
    

    const cout = productFilter?.product?.count;


    const handleInput = (e) => {
        const { value, isChecked } = e.target;

        setChecked(prev => {
            const isChecked = checked.includes(value);

            if (isChecked) {
                return checked.filter(item => item !== value);
            } else {
                return [...prev, value];
            }
        });
        
    }

    const handleCheckbox = (e) => {
        const { value } = e.target;
        setCheckbox(value);
    }

        filter.id = [...checked];
        filter.price = Number(checkbox);


    useEffect(() => {

        axios.post(`/api/filter-product/`, { filter })
            .then(res => setProductFilter(res))
            .catch(err => console.log(err))
    
    }, [checked, checkbox]);



    return (
        <Container className="filter">
            <Row>
                <Col className="col-3 filter-box">
                    <h4 className="title">Bộ lọc sản phẩm</h4>

                    <div>
                        <Form>
                            <Form.Label>Thương Hiệu</Form.Label>
                            <Form.Check
                                label="Tất cả"
                                name="price"
                                type="checkbox"
                                id="inline-checkbox-4"
                                value={0}
                                checked={checked.filter(check => check?.isChecked !== true).length < 1 || checked.length === brands.length + 1}
                                onChange={handleInput}
                            />
                            {brands?.map(brand => (
                                <Form.Check
                                    key={brand.id}
                                    label={brand.name}
                                    name="group1"
                                    type="checkbox"
                                    id={`inline-${brand.name}-1`}
                                    value={brand.id}
                                    onInput={handleInput}
                                />
                            ))}

                            <Form.Label>Giá</Form.Label>
                            <Form.Check
                                label="Tất cả"
                                name="price"
                                type="radio"
                                id="inline-radio-4"
                                value={0}
                                checked={checkbox === 0 || checkbox === '0'}
                                onChange={handleCheckbox}
                            />
                            <Form.Check
                                label="Dưới 5 triệu"
                                name="price"
                                type="radio"
                                id="inline-radio-1"
                                value={5000000}
                                onChange={handleCheckbox}
                            />
                            <Form.Check
                                label="Từ 5 triệu đến 20 triệu"
                                name="price"
                                type="radio"
                                id="inline-radio-2"
                                value={20000000}
                                onChange={handleCheckbox}
                            />
                            <Form.Check
                                label="Trên 20 triệu"
                                name="price"
                                type="radio"
                                id="inline-radio-3"
                                value={20111111}
                                onChange={handleCheckbox}
                            />
                            
                        </Form>
                    </div>
                </Col>

                <Col className="col-9">
                    {cout > 0 ? (
                        <Products
                            data={productFilter}
                            id="filter"
                        />
                    ) : (
                        <NotFound/>
                    )}
                </Col>
            </Row>
        </Container>
    );
}

export default FilterProducts;