import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Add({
    show = false,
    roles = [],
    category = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    const role = roles.roles.rows;

    const [lname, setLName] = useState();
    const [fname, setFName] = useState();
    const [image, setImage] = useState();
    const [address, setAddress] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [gender, setGender] = useState();
    const [roleId, setRoleId] = useState();
    const [password, setPassword] = useState();

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
                    <Modal.Header closeButton>
                        <Modal.Title>Thêm tài khoản</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form>
                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="lname">
                                <Form.Label>Họ</Form.Label>
                                <Form.Control type="text" value={lname} onChange={e => setLName(e.target.value)} />
                                </Form.Group>

                                <Form.Group as={Col} className="mb-3" controlId="fname">
                                <Form.Label>Tên</Form.Label>
                                <Form.Control type="text" value={fname} onChange={e => setFName(e.target.value)} />
                            </Form.Group>
                                
                            </Row>

                            <Form.Group controlId="image">
                                <Form.Label>Ảnh</Form.Label>
                                
                                <Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} />
                                <img width={200} src={image}/>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="address">
                                <Form.Label>Địa chỉ</Form.Label>
                                <Form.Control type="text" value={address} onChange={e => setAddress(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Mật khẩu</Form.Label>
                                <Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="phone">
                                <Form.Label>Số điện thoại</Form.Label>
                                <Form.Control type="number" value={phone} onChange={e => setPhone(e.target.value)} />
                            </Form.Group>

                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="gender">
                                    <Form.Label>Giới tính</Form.Label>
                                    <Form.Select value={gender} onChange={e => setGender(e.target.value)}>
                                        <option value={1}>Nam</option>
                                        <option value={0}>Nữ</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="roleId">
                                    <Form.Label>Quyền truy cập</Form.Label>
                                    <Form.Select value={roleId} onChange={e => setRoleId(e.target.value)}>
                                        {role?.map(item => (
                                            <option value={item.id} key={item.id} >{item.name}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Row>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Đóng
                        </Button>
                        <Button variant="primary" onClick={sendData}>Thêm</Button>
                    </Modal.Footer>
              
            
        </Modal>
    );
}

export default Add;