
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./SettingControl.scss";

function SettingControl({
    setting = [],
    openEdit = () => {},
    getValue = () => {},
    removeItem = () => {},
    openAdd = () => {},
}) {
    const sett = setting?.setting?.rows[0];
    return (
        <Container>
            <Row>
                <Col>
                    <h4 className="cate">Cấu hình website</h4>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Tên công ty</th>
                                <th>Logo</th>
                                <th>Địa chỉ</th>
                                <th>Email</th>
                                <th>SĐT</th>
                                <th>Fax</th>
                                <th>Mô tả</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{sett?.name}</td>
                                <td><img width={90} className="img-product" src={sett?.logo} alt={sett?.name}/></td>
                                <td>{sett?.address}</td>
                                <td>{sett?.email}</td>
                                <th>{sett?.phone}</th>
                                <th>{sett?.fax}</th>
                                <th>{sett?.description}</th>
                                <th>
                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                </th>
                            </tr>
                                        
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    );
}

export default SettingControl;