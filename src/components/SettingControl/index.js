import SettingControl from "./SettingControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default SettingControl;