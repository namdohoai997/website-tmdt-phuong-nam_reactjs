import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Edit({
    show = false,
    data = [],
    roles = [],
    brand = [],
    handleClose = () => {},
    sendData = () => {}
}) {
    console.log(data);
    const errCode = data?.errCode;
    const setting = data?.setting?.rows[0];
    // const roleData = roles?.roles?.rows;

    const [name, setName] = useState(setting?.name);
    const [logo, setLogo] = useState(setting?.logo);
    const [email, setEmail] = useState(setting?.email);
    const [phone, setPhone] = useState(setting?.phone);
    const [address, setAddress] = useState(setting?.address);
    const [description, setDescription] = useState(setting?.description);
    const [fax, setFax] = useState(setting?.fax);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
            {errCode === 0 ? (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin công ty</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId="id">
                                <Form.Control type="hidden" value={setting?.id} />
                                </Form.Group>

                            <Form.Group as={Col} controlId="name">
                                <Form.Label>Tên</Form.Label>
                                <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                            <Form.Group controlId="logo">
                                <Form.Label>Logo</Form.Label>
                                
                                <Form.Control type="text" value={logo} onChange={e => setLogo(e.target.value)} />
                                <img width={200} src={logo}/>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="address">
                                <Form.Label>Địa chỉ</Form.Label>
                                <Form.Control type="text" value={address} onChange={e => setAddress(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="phone">
                                <Form.Label>Số điện thoại</Form.Label>
                                <Form.Control type="number" value={phone} onChange={e => setPhone(e.target.value)} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="fax">
                                <Form.Label>Fax</Form.Label>
                                <Form.Control type="number" value={fax} onChange={e => setFax(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <Form.Control as="textarea" rows={3} value={description} onChange={e => setDescription(e.target.value)} />
                                {/* <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} /> */}
                            </Form.Group>


                            
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={sendData}>Xác nhận</Button>
                    </Modal.Footer>
                </>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Đã có lỗi xảy ra, vui lòng thử lại
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Xác nhận
                        </Button>
                    </Modal.Footer>
                </> 
            )}
        </Modal>
    );
}

export default Edit;