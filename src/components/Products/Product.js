import { Link } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';

function Product({ 
    product = {},
    priceAfterSale = 0,
    id = '',
    addToCart = () => {},
    limitText = () => {},
    convertMoneyFomat = () => {}
}) {
    return (
        product.is_active === 1 && (
            <Col key={product.id} className={id === 'filter' ? 'col-4 product' : 'col-3 product'}>
                <div className="product-box">
                    <div className="img-box">
                        <Link to={`/productDetail/${product.name}`} className="linkPro">
                            <img src={product.image} className="img-product" alt={product.name}/>
                        </Link>
                        {product.total_products > 0 ? (
                            <p className="add-to-cart" onClick={() => addToCart(product)}>
                                <FontAwesomeIcon icon={faCartShopping} className="icon"/>
                                Thêm vào giỏ hàng
                            </p>
                        ) : (
                            <p className="add-to-cart" >
                                Sản phẩm đã hết hàng
                            </p>
                        )}
                        
                    </div>
                    <Link to={`/productDetail/${product.name}`} className="linkPro">
                        <p className="name-product">{limitText(product.name)}</p>
                        <p className="price-product d-flex justify-content-between">
                            <span className={product.sale ? "price-sale" : "none"}>
                                {convertMoneyFomat(priceAfterSale)}
                            </span>
                            
                            <span className= {product.sale ? "text-remove" : 'price-sale'}>
                                {convertMoneyFomat(product.price)}
                            </span>
                        </p>
                        { product.sale > 0 && (
                            <p className="sale">-{product.sale}%</p>
                        ) }
                    </Link>

                    
                </div>
            </Col> 
        ) 
    )
}

export default Product;