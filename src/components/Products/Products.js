import { Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { useState } from "react";

import { addProductToCart } from "../../redux/apiRequest";
import Product from "./Product";

import "./Products.scss";


function Products({ 
    data = [],
    id = null
}) {
    // console.log(data);
    const products = data?.product ? data?.product : data?.products?.products;
    const user = useSelector((state) => state.auth.login.currentUser); 
    const productMap = products?.rows ? products.rows : products;
    const dispatch = useDispatch();


// console.log(data);
    const TEXT_LENGTH = 40;


    
    const handleLimitText = text => {
        let textEnd = text.length;
        let textCut = text.slice(TEXT_LENGTH, textEnd);

        if (textEnd > TEXT_LENGTH) {
            let newText = text.replace(textCut, '...');
            return newText;
        }

        return text;

    }

    const handleConvertMoneyFomat = price => {
        return price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
    }


    const addToCart = (product) => {
        if (user) {
            addProductToCart(dispatch, product);
        } else {
            toast.error("Bạn cần đăng nhập để sử dụng chức năng này!", {
                position: "bottom-right"
            });
        }
    }

    return (
            <>
            <Row>
                {products?.count ? ( 
                    <h3 className="number-found">Tìm thấy: {products?.rows?.filter(item => item.is_active === 1).length} sản phẩm</h3>
                ) : ( (products?.filter(item => item.id !== id && item.is_active === 1))?.length > 0 ?
                    (<h3 className="number-found">Sản phẩm cùng hãng</h3>) : (
                        ''
                    )
                )}
            </Row>

            <Row className="d-flex flex-wrap">
                { productMap?.map(product => {
                    let priceAfterSale = product.price - product.sale * product.price / 100;
                    if (id !== product.id)  {
                        return (
                            <Product
                                key={product.id}
                                product={product}
                                priceAfterSale={priceAfterSale}
                                id={id}
                                addToCart={addToCart}
                                limitText={handleLimitText}
                                convertMoneyFomat={handleConvertMoneyFomat}
                            />
                        ) 
                    }
                }) }
            </Row>
            </>
    );
}

export default Products;