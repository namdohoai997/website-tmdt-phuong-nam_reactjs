import Products from "./Products";
import NotFound from "./NotFound";
import Product from "./Product";

export { NotFound, Product };
export default Products;