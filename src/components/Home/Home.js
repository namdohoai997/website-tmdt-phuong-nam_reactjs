import { Col, Container, Row, Carousel, Pagination } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from 'react-paginate';

import "./Home.scss";
import Products from "../Products";
import Panigation from "../Panigation/Panigation";
import { useState } from "react";

function Home({ 
    banners = [],
    product = [],
    getValue = () => {}
}) { 
    // const products = product?.products?.products || product;
    // console.log(products);
    const pageCount = product?.total_page;
    const postsPerPgae = 2;

    const panigate = []

    for (let i = 1; i <= pageCount; i++) {
        if (!panigate.includes(i)) {
            panigate.push(i);
        }
    }

    return (
        <Container fluid="auto" className="home-container">
            <Row>
                <Col>
                    <Carousel fade>
                        {banners?.map(banner => (
                            banner?.is_active === 1 && <Carousel.Item key={banner.id}>
                                <img
                                    className="d-block w-100 banne-img"
                                    src={banner.image}
                                    alt={banner.title}
                                />
                                <Carousel.Caption>
                                <h3>{banner.title}</h3>
                                <p>{banner.description}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        ))}
                    </Carousel>
                </Col>
            </Row>

            <Row>
                <Col className="d-flex justify-content-center flex-column category">
                    <h3 className="d-flex justify-content-center cate-title">Danh Mục</h3>
                    <div className="nav d-flex justify-content-between">
                        <Link to="/laptop" className="link">Laptop</Link>
                        <Link to="/phone" className="link">Điện thoại</Link>
                        <Link to="/accessory" className="link">Phụ kiện</Link>
                    </div>
                </Col>
            </Row>

            <Container>
            <Col className="d-flex justify-content-center flex-column category">
                    <Row>
                        <h3 className="d-flex justify-content-center cate-title">Sản phẩm</h3>
                    </Row>

                    <Row>
                        <Col>
                            <Products data={product} />
                            <Pagination onClick={getValue} className="d-flex justify-content-center">
                                { panigate.map(item => (
                                    <Pagination.Item>{item}</Pagination.Item>
                                )) }
                            </Pagination>
                        </Col>
                    </Row>
                </Col>
            </Container>
        </Container>
    );
}

export default Home;