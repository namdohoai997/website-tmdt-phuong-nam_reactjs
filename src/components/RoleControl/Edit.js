import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Edit({
    show = false,
    data = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    console.log(data);
    const errCode = data?.errCode;
    const role = data?.role;

    // const [active, setActive] = useState(banners?.is_active);
    // const [title, setTitle] = useState(banners?.title);
    const [name, setName] = useState(role?.name);
    const [description, setDescription] = useState(role?.description);

    // console.log(active);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
            {errCode === 0 ? (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin Banner</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId="id">
                                <Form.Control type="hidden" value={role?.id} />
                                </Form.Group>

                                <Form.Group controlId="name">
                                <Form.Label>Tên</Form.Label>
                                <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                                <Form.Group controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <Form.Control type="tex" value={description} onChange={e => setDescription(e.target.value)} />
                                </Form.Group>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={sendData}>Xác nhận</Button>
                    </Modal.Footer>
                </>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Đã có lỗi xảy ra, vui lòng thử lại
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Xác nhận
                        </Button>
                    </Modal.Footer>
                </>
            )}
        </Modal>
    );
}

export default Edit;