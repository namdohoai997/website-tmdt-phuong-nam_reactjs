import RoleControl from "./RoleControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default RoleControl;