import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Add({
    show = false,
    handleClose = () => {},
    sendData = () => {}
}) {

    const [name, setName] = useState();
    const [description, setDescription] = useState();

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
                    <Modal.Header closeButton>
                        <Modal.Title>Thêm danh mục</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form>

                                <Form.Group controlId="name">
                                <Form.Label>Tên</Form.Label>
                                <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                                <Form.Group controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <Form.Control type="tex" value={description} onChange={e => setDescription(e.target.value)} />
                                </Form.Group>
                            </Form>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Đóng
                        </Button>
                        <Button variant="primary" onClick={sendData}>Thêm</Button>
                    </Modal.Footer>
              
            
        </Modal>
    );
}

export default Add;