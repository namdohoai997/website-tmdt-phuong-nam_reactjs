
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./RoleControl.scss";

function RoleControl({
    role = [],
    openEdit = () => {},
    removeItem = () => {},
    openAdd = () => {}
}) {

    // console.log(role);
    const errCode = role?.errCode;

    const [keyword, setKeyword] = useState('');


    const roles = role?.roles?.rows;
    if (errCode === 0) {
        return (
        
            <Container>
                <Row>
                    <Col>
                        <Row className="add-btn">
                            <Col>
                                <Form>
                                    <Form.Group className="mb-3" controlId="search">
                                        <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                    </Form.Group>
                                </Form>
    
                            </Col>
    
                            <Col>
                                <Button  onClick={openAdd}>Thêm danh mục</Button>
                            </Col>
                        </Row>
                        <h4 className="cate">Quản lí quyền</h4>
                    
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Mã banner</th>
                                    <th>Tên</th>
                                    <th>Mô tả</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                    {roles?.map(role => {
                                            return (
                                                <tr key={role.id}>
                                                    <td>{role.id}</td>
                                                    <td>{role.name}</td>
                                                    <td>{role.description}</td>
                                                    <th>
                                                        <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                        <Button className="remove-btn" variant="danger" onClick={removeItem}>Xóa</Button>
                                                    </th>
                                                </tr>
                                            )
                                    }) 
                         
                                }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        )
    } else {
        return <h3>Có lỗi xảy ra</h3>
    }
}

export default RoleControl;