import { Row, Col, Container, Form, Button } from "react-bootstrap";
import axios from "axios";
import { useEffect, useRef, useState } from "react";

import UserProvider from "../../containers/UserProvider";
import { loginUser, registerUser } from "../../redux/apiRequest";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

function Account() {
    const [userData, setUserData] = useState({});
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [gender, setGender] = useState(null);
    const [selectedFile, setSelectedFile] = useState(null);
    const [preview, setPreview] = useState();
    const dispatch = useDispatch();
    const navigate = useNavigate();



    const handleLogin = async(e) => {
        e.preventDefault();

        let { email, password } = document.forms[1];

        const data = {
            email: email.value,
            password: password.value
        }

        loginUser(data, dispatch, navigate);
        }

        const handleRegister = (e) => {
            e.preventDefault();
            const { 
                fname,
                lname,
                address,
                email, 
                pnumber,
                userName,
                password,
                avatar
            } = document.forms[2];

            const data = {
                firstName: fname.value,
                lastName: lname.value,
                address: address.value,
                email: email.value,
                gender: gender === "1" ? true : false,
                userName: userName.value,
                password: password.value,
                phoneNumber: pnumber.value,
                roleId: 6,
                image: preview
            }

            registerUser(data, dispatch);
        }

        useEffect(() => {
            if (!selectedFile) {
                setPreview(undefined)
                return
            }
    
            const objectUrl = URL.createObjectURL(selectedFile)
            setPreview(objectUrl)
    
            return () => URL.revokeObjectURL(objectUrl)
        }, [selectedFile])

        const handleAvatar = (e) => {
            if (!e.target.files || e.target.files.length === 0) {
                setSelectedFile(undefined)
                return
            }
    
            setSelectedFile(e.target.files[0])
        }

    return (
        <Container>
            <Row>
                <Col>
                    <h2>Đăng Nhập</h2>
                    <Form onSubmit={handleLogin}>
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email" 
                                name="email" 
                                onChange={e => setEmail(e.target.value)}    
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                name="password" 
                                onChange={e => setPassword(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>

                <Col>
                    <h2>Đăng Kí</h2>
                    <Form onSubmit={handleRegister}>
                        <Form.Group className="mb-3" controlId="fname">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="text" placeholder="First Name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="lname">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control type="text" placeholder="Last Name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="address">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" placeholder="Address" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>
                        <Form.Select aria-label="Default select example" onChange={e => setGender(e.target.value)}>
                            <option value="null">Giới tính</option>
                            <option value="1">Nam</option>
                            <option value="0">Nữ</option>
                        </Form.Select>
                        
                        <Form.Group className="mb-3" controlId="userName">
                            <Form.Label>User Name</Form.Label>
                            <Form.Control type="text" placeholder="User Name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="pnumber">
                            <Form.Label>Phone Number</Form.Label>
                            <Form.Control type="number" placeholder="Phone Number" />
                        </Form.Group>
                        <Form.Group controlId="avatar" className="mb-3">
                            <Form.Label>Avatar</Form.Label>
                            <Form.Control 
                                type="file" 
                                onChange={e => handleAvatar(e)}
                            />
                            {selectedFile && <img src={preview} width={200}/>}
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

export default Account;