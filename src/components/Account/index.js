import Account from "./Account";

import Login from "./Login";
import Cart from "./Cart";
import Destroy from "./Destroy";
import Register from "./Register";
import Confirm from "./Confirm";
import OrderDetail from "./OrderDetail";

export { Login, Cart, Destroy, Register, Confirm, OrderDetail }

export default Account;