import { Button, Modal } from "react-bootstrap";

function Confirm({
    show = false,
    handleClose = () => {},
    handleConfirm = () => {}
}) {
    return (
        <>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Thông báo</Modal.Title>
            </Modal.Header>
            <Modal.Body>Bạn có muốn đăng nhập không?</Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Hủy bỏ
              </Button>
              <Button variant="primary" onClick={handleConfirm}>
                Đồng ý
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      );
    }

export default Confirm;