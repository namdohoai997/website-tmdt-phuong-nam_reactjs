import { Modal, Table, Button } from "react-bootstrap";

function OrderDetail({
    display = false,
    detailList = [],
    detroyOrder = () => {},
    handleDisplay = () => {}
}) {
    return (
        <Modal
            show={display}
            size="lg"
            onHide={handleDisplay}
            dialogClassName="modal-90w"
            aria-labelledby="example-modal-sizes-title-lg"
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-custom-modal-styling-title">
                Chi tiết đơn hàng
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                    <th>Mã ĐH</th>
                    <th>Sản phẩm</th>
                    <th>Giá tiền</th>
                    <th>Số lượng</th>
                    <th>Tổng tiền</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    {detailList?.orderDetail?.map(item => (
                        <tr key={item.id}>
                            <td>{item.order_id}</td>
                            <td>{item.product}</td>
                            <td>{item.unit_price}</td>
                            <td>{item.quantity}</td>
                            <td>{item.total_price}</td>
                            <td>{item.status}</td>
                            { (item.order_status === 1) ?
                              (<td>
                                {item.order_status !== 4 ? <Button variant="danger" onClick={() => detroyOrder(item.id, item.product_id, item.quantity)}>Hủy đơn hàng</Button> : <p>Đơn hàng đã hủy</p>}
                              </td>) : (<td></td>)
                            }
                        </tr>
                    ))}
                </tbody>
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleDisplay}>Quay lại</Button>
            </Modal.Footer>
          </Modal>
    );
}

export default OrderDetail;