import axios from "axios";
import { useEffect, useState } from "react";
import { Modal, Button, Table } from "react-bootstrap";

function Cart({
    data = [],
    transports = [],
    display = false,
    setShow = () => {},
    getValue = () => {}
}) {
    return (
            <Modal
            show={display}
            onHide={() => setShow(false)}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
            fullscreen={true}
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-custom-modal-styling-title">
                Giỏ hàng của bạn
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                    <th>Mã ĐH</th>
                    <th>Mô tả</th>
                    <th>Tổng tiền</th>
                    <th>Đơn vị vân chuyển</th>
                    <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    {data?.map(item => (
                        <tr key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.description}</td>
                            <td>{item.total_Orders}</td>
                            <td>{transports?.filter(transport => transport.id === item?.transport_id)[0]?.name}</td>
                            <td>
                              <Button variant="primary" onClick={getValue}>Xem</Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={setShow}>Close</Button>
            </Modal.Footer>
          </Modal>
      );
}

export default Cart;