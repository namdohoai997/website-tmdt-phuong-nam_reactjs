// import { useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";

import { useEffect, useRef, useState } from "react";

import { loginUser, registerUser } from "../../redux/apiRequest";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";


function Login({
    show = false, 
    closeButton = () => {},
    loginButton = () => {},
    registerButton = () => {}
}) {
    return (
            <Modal 
            show={show} 
            onHide={closeButton}
            backdrop="static"
            keyboard={false}
            centered
            aria-labelledby="contained-modal-title-vcenter"
        >
          <Modal.Header closeButton>
            <Modal.Title>Đăng nhập</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="name@example.com"
                  autoFocus
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="password"
              >
                <Form.Label>Mật khẩu</Form.Label>
                <Form.Control
                  type="password"
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
          <Button variant="link" onClick={registerButton}>
              Đăng kí
            </Button>
            <Button variant="secondary" onClick={closeButton}>
              Đóng
            </Button>
            <Button variant="primary" type="submit" onClick={loginButton}>
              Đăng Nhập
            </Button>
          </Modal.Footer>
        </Modal>
    );
  }

  export default Login