import { useState } from "react";
import validator from 'validator';
import { Modal, Form, Button } from "react-bootstrap";

function Register({
    show = false,
    selectedFile = false,
    preview = '',
    registerButton = () => {},
    setGender = () => {},
    handleAvatar = () => {},
    closeButton = () => {},
    loginButton = () => {},
}) {

    const [emailError, setEmailError] = useState('');
    const [email, setEmail] = useState(false);

    const validateEmail = (e) => {
      var email = e.target.value;

      if (validator.isEmail(email)) {
        setEmailError('Valid Email');
        setEmail(true);
      } else {
        setEmailError('Enter valid Email!');
        setEmail(false);
      }
    }
    return (
        <Modal 
            show={show} 
            size="xl"
            onHide={closeButton}
            backdrop="static"
            keyboard={false}
            centered
            aria-labelledby="contained-modal-title-vcenter"
        >
          <Modal.Header closeButton>
            <Modal.Title>Đăng Kí</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
            <Form.Group className="mb-3" controlId="fname">
                <Form.Label>First Name</Form.Label>
                    <Form.Control type="text" placeholder="First Name" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="lname">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="text" placeholder="Last Name" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="address">
                    <Form.Label>Address</Form.Label>
                    <Form.Control type="text" placeholder="Address" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                      type="email" 
                      placeholder="Enter email" 
                      onChange={(e) => validateEmail(e)}
                    />
                    <span style={{
                      fontWeight: 'bold',
                      color: 'red',
                    }}>{emailError}</span>
                </Form.Group>
                <Form.Select aria-label="Default select example" onChange={e => setGender(e.target.value)}>
                    <option>Giới tính</option>
                    <option value="1">Nam</option>
                    <option value="0">Nữ</option>
                </Form.Select>
                
                <Form.Group className="mb-3" controlId="userName">
                    <Form.Label>User Name</Form.Label>
                    <Form.Control type="text" placeholder="User Name" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="pnumber">
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control type="number" placeholder="Phone Number" />
                </Form.Group>
                <Form.Group controlId="avatar" className="mb-3">
                    <Form.Label>Avatar</Form.Label>
                    <Form.Control 
                        type="file" 
                        onChange={e => handleAvatar(e)}
                    />
                    {selectedFile && <img src={preview} width={200}/>}
                </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
          <Button variant="link" onClick={loginButton}>
              Đăng nhập
            </Button>
            <Button variant="secondary" onClick={closeButton}>
              Đóng
            </Button>
            <Button variant="primary" onClick={registerButton} disabled={!email ? 'disabled' : ''}>
              Đăng kí
            </Button>
          </Modal.Footer>
        </Modal>
    );
}

export default Register;