import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Add({
    show = false,
    data = [],
    category = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    const errCode = data?.errCode || category?.errCode;
    const brand = data?.brand;
    const categories = category.cates;
    // const brands = brand?.brands.rows;

    const [name, setName] = useState();
    const [active, setActive] = useState();
    const [catePro, setCatePro] = useState();
    const [description, setDescription] = useState();

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
                    <Modal.Header closeButton>
                        <Modal.Title>Thêm sản phẩm</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form>
                                <Form.Group controlId="name">
                                <Form.Label>Tên thương hiệu</Form.Label>
                                <Form.Control type="name" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                            <Row className="mb-3">

                                <Form.Group as={Col} controlId="active">
                                    <Form.Label>Trạng thái</Form.Label>
                                    <Form.Select value={active} onChange={e => setActive(e.target.value)}>
                                        <option value={1} selected={active === 1 ? 'selected' : ''}>Hiện thị</option>
                                        <option value={0} selected={active === 0 ? 'selected' : ''}>Ẩn</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="category">
                                    <Form.Label>Danh mục</Form.Label>
                                    <Form.Select value={catePro} onChange={e => setCatePro(e.target.value)}>
                                        {categories?.map(item => (
                                            <option value={item.id} key={item.id} selected={catePro === Number(item.id) ? 'selected' : ''}>{item.name}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Row>

                            <Form.Group as={Col} controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    value={description}
                                    data={description}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        setDescription(data)
                                    } }
                                />
                            </Form.Group>
                            </Form>
                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Đóng
                        </Button>
                        <Button variant="primary" onClick={(e) => sendData(e, description)}>Thêm</Button>
                    </Modal.Footer>
              
            
        </Modal>
    );
}

export default Add;