import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Edit({
    show = false,
    data = [],
    category = [],
    handleClose = () => {},
    sendData = () => {}
}) {

    const errCode = data?.errCode || category?.errCode;
    const brand = data?.brand;
    const categories = category.cates;
    // const brands = brand?.brands.rows;

    const [name, setName] = useState(brand?.name);
    const [active, setActive] = useState(brand?.is_active);
    const [catePro, setCatePro] = useState(brand?.category_id);
    const [description, setDescription] = useState(brand?.description);

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
            {errCode === 0 ? (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông tin sản phẩm</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId="id">
                                <Form.Control type="hidden" value={brand?.id} />
                                </Form.Group>
                                <Form.Group controlId="name">
                                <Form.Label>Tên thương hiệu</Form.Label>
                                <Form.Control type="name" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                            <Row className="mb-3">

                                <Form.Group as={Col} controlId="active">
                                    <Form.Label>Trạng thái</Form.Label>
                                    <Form.Select value={active} onChange={e => setActive(e.target.value)}>
                                        <option value={1} selected={active === 1 ? 'selected' : ''}>Hiện thị</option>
                                        <option value={0} selected={active === 0 ? 'selected' : ''}>Ẩn</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="category">
                                    <Form.Label>Danh mục</Form.Label>
                                    <Form.Select value={catePro} onChange={e => setCatePro(e.target.value)}>
                                        {categories?.map(item => (
                                            <option value={item.id} key={item.id} selected={catePro === Number(item.id) ? 'selected' : ''}>{item.name}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Row>

                            <Form.Group as={Col} controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    value={description}
                                    data={description}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        setDescription(data)
                                    } }
                                />
                            </Form.Group>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={(e) => sendData(e, description)}>Xác nhận</Button>
                    </Modal.Footer>
                </>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Đã có lỗi xảy ra, vui lòng thử lại
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Xác nhận
                        </Button>
                    </Modal.Footer>
                </>
            )}
        </Modal>
    );
}

export default Edit;