
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";

import "./BrandControl.scss";

function BrandControl({
    brands = [],
    openEdit = () => {},
    getValue = () => {},
    removeBrand = () => {},
    openAdd = () => {}
}) {

    const [keyword, setKeyword] = useState('');

    const panigate = []

    for (let i = 1; i <= brands?.total_page; i++) {
        if (!panigate.includes(i)) {
            panigate.push(i);
        }
    }



    const brandList = brands?.brands?.brands;

    return (
        <Container>
            <Row>
                <Col>
                    <Row className="add-btn">
                        <Col>
                            <Form>
                                <Form.Group className="mb-3" controlId="search">
                                    <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                </Form.Group>
                            </Form>

                        </Col>

                        <Col>
                            <Button  onClick={openAdd}>Thêm thương hiệu</Button>
                        </Col>
                    </Row>

                    <h4 className="cate">Quản lí thương hiệu</h4>
                
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Mã SP</th>
                                <th>Tên thương hiệu</th>
                                <th>Trạng thái</th>
                                <th>Danh mục</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                                {brandList?.map(brand => {
                                    
                                    if (keyword === "") {
                                        return (
                                            <tr key={brand.id}>
                                                <td>{brand.id}</td>
                                                <td>{brand.name}</td>
                                                <td>{brand.is_active === 1 ? 'Hiển thị' : 'Ẩn'}</td>
                                                <th>{brand.cate_name}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeBrand}>Xóa</Button>
                                                </th>
                                            </tr>
                                        )
                                    } else if (brand?.name?.includes(keyword.toLocaleLowerCase())) {
                                        return (
                                            <tr key={brand.id}>
                                                <td>{brand.id}</td>
                                                <td>{brand.name}</td>
                                                <td>{brand.is_active === 1 ? 'Hiển thị' : 'Ẩn'}</td>
                                                <th>{brand.cate_name}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeBrand}>Xóa</Button>
                                                </th>
                                            </tr>
                                        );
                                    }
                                })
                    
                            }
                        </tbody>

                        <tfoot>
                            <Pagination onClick={getValue} className="d-flex justify-content-center">
                                { panigate.map(item => (
                                    <Pagination.Item>{item}</Pagination.Item>
                                )) }
                            </Pagination>
                        </tfoot>
                    </Table>
                </Col>
            </Row>
        </Container>
    );
}

export default BrandControl;