import BrandControl from "./BrandControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default BrandControl;