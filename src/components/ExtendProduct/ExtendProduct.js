import { useState } from "react";
import { Collapse, Button } from "react-bootstrap";
import DOMPurify from "dompurify";

import "./ExtendProduct.scss";

function ExtendProduct({ data }) {
    const [open, setOpen] = useState(false);
    const myHTML = data;

    const mySafeHTML = DOMPurify.sanitize(myHTML);

    return (
        <>
            <Collapse in={open}>
                <div id="example-collapse-text" className="content">
                    {/* {data}
                     */}
                     <div dangerouslySetInnerHTML={{ __html: mySafeHTML }} />
                </div>
            </Collapse>
            {open ? (
                <Button
                    onClick={() => setOpen(false)}
                    aria-controls="example-collapse-text"
                    aria-expanded={open}
                    size="sm"
                    className="collap"
                    >
                    Thu nhỏ
                </Button>
            ) : (
                <Button
                    onClick={() => setOpen(true)}
                    aria-controls="example-collapse-text"
                    aria-expanded={open}
                    size="sm"
                    className="extend"
                >
                Mở rộng
                </Button>)
            }


        </>
    );
}

export default ExtendProduct;