import { Modal, Button, Form, Col, Row } from "react-bootstrap";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from "react";

function Add({
    show = false,
    brand = [],
    category = [],
    handleClose = () => {},
    sendData = () => {}
}) {
    const brands = brand.brands.brands;
    const categories = category.cates;
    console.log(brands);

    const [name, setName] = useState();
    const [image, setImage] = useState();
    const [price, setPrice] = useState();
    const [quantity, setQuantity] = useState();
    const [hot, setHot] = useState();
    const [active, setActive] = useState();
    const [brandPro, setBrandPro] = useState();
    const [catePro, setCatePro] = useState();
    const [sale, setSale] = useState();
    const [description, setDescription] = useState();

    return (
        <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            fullscreen={true}
        >
                    <Modal.Header closeButton>
                        <Modal.Title>Thêm sản phẩm</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="name">
                                <Form.Label>Tên sản phẩm</Form.Label>
                                <Form.Control type="name" value={name} onChange={e => setName(e.target.value)} />
                                </Form.Group>

                                <Form.Group as={Col} controlId="image">
                                <Form.Label>Ảnh</Form.Label>
                                
                                <Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} />
                                <img width={200} src={image}/>
                                </Form.Group>
                            </Row>

                            <Form.Group className="mb-3" controlId="price">
                                <Form.Label>Giá</Form.Label>
                                <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="quantity">
                                <Form.Label>Số lượng</Form.Label>
                                <Form.Control type="number" value={quantity} onChange={e => setQuantity(e.target.value)} />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="sale">
                                <Form.Label>Giảm giá</Form.Label>
                                <Form.Control type="number" value={sale} onChange={e => setSale(e.target.value)} />
                            </Form.Group>

                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="hot">
                                    <Form.Label>Hot</Form.Label>
                                    <Form.Select value={hot} onChange={e => setHot(e.target.value)}>
                                        <option value={1} selected={hot === 1 ? 'selected' : ''}>Có</option>
                                        <option value={0} selected={hot === 0 ? 'selected' : ''}>Không</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="active">
                                    <Form.Label>Trạng thái</Form.Label>
                                    <Form.Select value={active} onChange={e => setActive(e.target.value)}>
                                        <option value={1} selected={active === 1 ? 'selected' : ''}>Hiện thị</option>
                                        <option value={0} selected={active === 0 ? 'selected' : ''}>Ẩn</option>
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="brand">
                                    <Form.Label>Thương hiệu</Form.Label>
                                    <Form.Select value={brandPro} onChange={e => setBrandPro(e.target.value)}>
                                        {brands?.map(item => (
                                            <option value={item.id} key={item.id} selected={brandPro === Number(item.id) ? 'selected' : ''}>{item.name}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>

                                <Form.Group as={Col} controlId="category">
                                    <Form.Label>Danh mục</Form.Label>
                                    <Form.Select value={catePro} onChange={e => setCatePro(e.target.value)}>
                                        {categories?.map(item => (
                                            <option value={item.id} key={item.id} selected={catePro === Number(item.id) ? 'selected' : ''}>{item.name}</option>
                                        ))}
                                    </Form.Select>
                                </Form.Group>
                            </Row>

                            <Form.Group as={Col} controlId="description">
                                <Form.Label>Mô tả</Form.Label>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    value={description}
                                    data={description}
                                    config={{ckfinder: {
                                        // Upload the images to the server using the CKFinder QuickUpload command
                                        // You have to change this address to your server that has the ckfinder php connector
                                        uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json'
                                    }}}
                                    onReady={ editor => {
                                        console.log( 'Editor is ready to use!', editor );
                                    } }
                                    onChange={ ( event, editor ) => {
                                        const data = editor.getData();
                                        setDescription(data)
                                    } }
                                />
                            </Form.Group>
                            </Form>

                        
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Đóng
                        </Button>
                        <Button variant="primary" onClick={(e) => sendData(e, description)}>Thêm</Button>
                    </Modal.Footer>
              
            
        </Modal>
    );
}

export default Add;