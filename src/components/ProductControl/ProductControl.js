
import { useState } from "react";
import { Container, Row, Col, Table, Button, Pagination, Form } from "react-bootstrap";
// import ReactExport from 'react-data-export';
import * as XLSX from 'xlsx';

import "./ProductControl.scss";

function ProductControl({
    products = [],
    openEdit = () => {},
    getValue = () => {},
    removeProduct = () => {},
    openAdd = () => {},
    searchData = () => {}
}) {


    const [keyword, setKeyword] = useState('');

    const productList = products?.products?.products;

    const panigate = [];

    for (let i = 1; i <= products?.total_page; i++) {
        if (!panigate.includes(i)) {
            panigate.push(i);
        }
    }

    const handleExport = () => {
        let wb = XLSX.utils.book_new();
        let ws = XLSX.utils.json_to_sheet(productList);

        XLSX.utils.book_append_sheet(wb, ws, "MySheet1");

        XLSX.writeFile(wb, "MyExcel.xlsx");
    }
    
    
    return (
        <Container>
            
            <Row>
                <Col>
                
                    <Row className="add-btn">
                        <Col>
                            <Form>
                                <Form.Group className="mb-3" controlId="search">
                                    <Form.Control type="text" placeholder="Search" onChange={e => setKeyword(e.target.value)} />
                                </Form.Group>
                            </Form>

                        </Col>

                        <Col>
                            <Button  onClick={openAdd}>Thêm sản phẩm</Button>
                        </Col>
                    </Row>
                    <h4 className="cate">Quản lí sản phẩm</h4>
                
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Mã SP</th>
                                <th>Tên SP</th>
                                <th>Ảnh</th>
                                <th>Hãng</th>
                                <th>Số lượng</th>
                                <th>Giá</th>
                                <th>Sale</th>
                                <th>Hot</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                                {productList?.map(product => {
                                    if (keyword === "") {
                                        return (
                                            <tr key={product.id}>
                                                <td>{product.id}</td>
                                                <td>{product.name}</td>
                                                <td><img className="img-product" src={product.image} alt={product.name}/></td>
                                                <td>{product.brand_name}</td>
                                                <td>{product.total_products}</td>
                                                <th>{Number(product.price).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</th>
                                                <th>{product.sale}%</th>
                                                <th>{product.is_hot === 1 ? 'Hot' : 'Không'}</th>
                                                <th>{product.is_active === 1 ? 'Hiển thị' : 'Không'}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeProduct}>Xóa</Button>
                                                </th>
                                            </tr>
                                        )
                                    } else if (product.slug.includes(keyword.toLocaleLowerCase())) {
                                        return (
                                            <tr key={product.id}>
                                                <td>{product.id}</td>
                                                <td>{product.name}</td>
                                                <td><img className="img-product" src={product.image} alt={product.name}/></td>
                                                <td>{product.brand_name}</td>
                                                <th>{Number(product.price).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</th>
                                                <th>{product.sale}%</th>
                                                <th>{product.is_hot === 1 ? 'Hot' : 'Không'}</th>
                                                <th>
                                                    <Button className="edit-btn" variant="primary" onClick={openEdit}>Sửa</Button>
                                                    <Button className="remove-btn" variant="danger" onClick={removeProduct}>Xóa</Button>
                                                </th>
                                            </tr>
                                        );
                                    }
                                })
                    
                            }
                        </tbody>

                        <tfoot>
                            <Pagination onClick={getValue} className="d-flex justify-content-center">
                                { panigate.map(item => (
                                    <Pagination.Item>{item}</Pagination.Item>
                                )) }
                            </Pagination>
                        </tfoot>
                    </Table>
                   <Button onClick={handleExport}>Export Excel</Button>
                </Col>
            </Row>
        </Container>
    );
}

export default ProductControl;