import ProductControl from "./ProductControl";

import Edit from "./Edit";
import Add from "./Add";

export { Edit, Add };

export default ProductControl;