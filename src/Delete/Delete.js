import { Modal, Button } from "react-bootstrap";

function Delete({
    data = null,
    show = false,
    cancelDelete = () => {},
    confirmDelete = () => {}
}) {
    return (
        <>
            <Modal
                show={show}
                onHide={cancelDelete}
                backdrop="static"
                keyboard={false}
                centered
            >
                <Modal.Header closeButton>
                <Modal.Title>Thông Báo</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Bạn có muốn xóa {data} này không?
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={cancelDelete}>
                    Hủy Bỏ
                </Button>
                <Button variant="primary" onClick={confirmDelete}>Đồng Ý</Button>
                </Modal.Footer>
            </Modal>
            </>
    );
}

export default Delete;