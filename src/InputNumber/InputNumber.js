
import { useRef, useState } from "react";
import "./InputNumber";

function InputNumber({
    value,
    maxProduct,
    minProduct,
    decrease = () => {},
    increase = () => {},
    channgeValue = () => {},
}) {
    // const [disable, setDisable] = useState(false);

    // console.log(maxProduct);

    const inputRef = useRef();

    // const channgeValue = (e) => {
    //     console.log(e.target.value);
    // }

    // console.log('input value: ', inputRef?.current?.value);
    // console.log('max: ', maxProduct);

    return (
        <div className="def-number-input number-input">
            <button onClick={decrease} className="minus">-</button>
            <input
                className="quantity"
                value={value}
                ref={inputRef}
                onChange={channgeValue}
                type="number"
                max={maxProduct}
                min={minProduct}
            />
            <button onClick={increase} disabled={maxProduct < inputRef?.current?.value ? 'disabled' : ''} className="plus">+</button>
        </div>
    );
}

export default InputNumber;