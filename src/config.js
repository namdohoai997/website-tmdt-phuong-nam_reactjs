const config = {
    mainWidth: 1100,
    routes: {
        home: '/',
        notification: '/noti',
        cart: '/cart',
        account: '/account',
        search: '/search/:slug/',
        productDetail: 'productDetail/:slug/',
        phone: '/phone',
        laptop: '/laptop',
        accessory: '/accessory',
        admin: '/admin',
        productControl: '/admin/product',
        statistical: '/admin/statistical',
        orderControl: '/admin/order',
        accountControl: '/admin/account',
        brandControl: '/admin/brand',
        categoryControl: '/admin/category',
        bannerControl: '/admin/banner',
        roleControl: '/admin/role',
        settingControl: '/admin/setting',
        transportControl: '/admin/transport'
    },
    socials: {
        shares: {
            whatsapp: url => `https://api.whatsapp.com/send/?text=${encodeURIComponent(url)}`,
            facebook: url => `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(url)}`,
            twitter: url => `https://twitter.com/intent/tweet?refer_source=${encodeURIComponent(url)}`,
        }
    }
}

export default config
